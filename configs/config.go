package configs

import (
	"context"
	"os"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gopkg.in/yaml.v2"
)

// Config struct that contians the structure of the config
type Config struct {
	Redis struct {
		Host   string `yaml:"host"`
		Port   int    `yaml:"port"`
		Pool   int    `yaml:"pool"`
		Prefix string `yaml:"prefix"`
	} `yaml:"REDIS"`
	DB struct {
		Type string `yaml:"type"`
	} `yaml:"DB"`
	SQLite3 struct {
		Path   string `yaml:"path"`
		DBName string `yaml:"db_name"`
	} `yaml:"SQLITE3"`
	PostgreSQL struct {
		Host   string `yaml:"host"`
		Port   string `yaml:"port"`
		DBName string `yaml:"db_name"`
	} `yaml:"POSTGRESQL"`
	NitradoService struct {
		Host     string `yaml:"host"`
		BasePath string `yaml:"base_path"`
	} `yaml:"NITRADO_SERVICE"`
	CacheSettings struct {
		BoostSetting               CacheSetting `yaml:"boost_setting"`
		Contact                    CacheSetting `yaml:"contact"`
		GuildServiceContact        CacheSetting `yaml:"guild_service_contact"`
		GuildServicePermission     CacheSetting `yaml:"guild_service_permission"`
		GuildService               CacheSetting `yaml:"guild_service"`
		Guild                      CacheSetting `yaml:"guild"`
		GuildFeed                  CacheSetting `yaml:"guild_feed"`
		NitradoToken               CacheSetting `yaml:"nitrado_token"`
		OutputChannelType          CacheSetting `yaml:"output_channel_type"`
		ServerOutputChannel        CacheSetting `yaml:"server_output_channel"`
		ServerOutputChannelSetting CacheSetting `yaml:"server_output_channel_setting"`
		GuildOutputChannel         CacheSetting `yaml:"guild_output_channel"`
		ServerType                 CacheSetting `yaml:"server_type"`
		Server                     CacheSetting `yaml:"server"`
		Setup                      CacheSetting `yaml:"setup"`
		ArkTribe                   CacheSetting `yaml:"ark_tribe"`
		ArkTribeMember             CacheSetting `yaml:"ark_tribe_member"`
	} `yaml:"CACHE_SETTINGS"`
}

// CacheSetting struct
type CacheSetting struct {
	Base    string `yaml:"base"`
	TTL     string `yaml:"ttl"`
	Enabled bool   `yaml:"enabled"`
}

// GetConfig gets the config file and returns a Config struct
func GetConfig(ctx context.Context, env string) *Config {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	configFile := "./configs/conf-" + env + ".yml"
	f, err := os.Open(configFile)

	if err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	defer f.Close()

	var config Config
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&config)

	if err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	return &config
}
