package controllers

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// GetAllContacts route
func (c *Controller) GetAllContacts(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	model := models.Contact{}
	records, err := model.FindAll(ctx, c.DB)
	if err != nil {
		Error(ctx, w, err.Message, err.Err, http.StatusBadRequest)
		return
	}

	Response(ctx, w, viewmodels.GetAllContactsResponse{
		Message:  "Found records",
		Contacts: *records,
	}, http.StatusOK)
}

// GetContactByID route
func (c *Controller) GetContactByID(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	vars := mux.Vars(r)
	id := vars["contact_id"]

	var modelCache *models.Contact
	getCacheErr := GetCache(ctx, c.Cache, c.Config.CacheSettings.Contact, id, &modelCache)
	if getCacheErr != nil {
		Error(ctx, w, getCacheErr.Message, getCacheErr.Err, http.StatusInternalServerError)
		return
	}

	if modelCache != nil {
		w.Header().Set("X-Cache", "HIT")
		Response(ctx, w, viewmodels.GetContactByIDResponse{
			Message: "Found record",
			Contact: *modelCache,
		}, http.StatusOK)
		return
	}

	model := models.Contact{}
	record, err := model.FindByID(ctx, c.DB, id)
	if err != nil {
		if err.Err == gorm.ErrRecordNotFound {
			Error(ctx, w, err.Message, err.Err, http.StatusNotFound)
			return
		}
		Error(ctx, w, err.Message, err.Err, http.StatusBadRequest)
		return
	}

	setCacheErr := SetCache(ctx, c.Cache, c.Config.CacheSettings.Contact, id, record)
	if setCacheErr != nil {
		Error(ctx, w, setCacheErr.Message, setCacheErr.Err, http.StatusInternalServerError)
		return
	}

	w.Header().Set("X-Cache", "MISS")
	Response(ctx, w, viewmodels.GetContactByIDResponse{
		Message: "Found record",
		Contact: *record,
	}, http.StatusOK)
}

// CreateContact route
func (c *Controller) CreateContact(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	var body models.Contact
	dcErr := json.NewDecoder(r.Body).Decode(&body)
	if dcErr != nil {
		Error(ctx, w, "Failed to decode request body", dcErr, http.StatusBadRequest)
		return
	}

	record, err := body.Create(ctx, c.DB)
	if err != nil {
		Error(ctx, w, err.Message, err.Err, http.StatusBadRequest)
		return
	}

	expCacheErr := ExpireCache(ctx, c.Cache, c.Config.CacheSettings.GuildFeed, r.Header.Get("Guild"))
	if expCacheErr != nil {
		Error(ctx, w, expCacheErr.Message, expCacheErr.Err, http.StatusInternalServerError)
		return
	}

	Response(ctx, w, viewmodels.CreateContactResponse{
		Message: "Created record",
		Contact: *record,
	}, http.StatusCreated)
}

// UpdateContact route
func (c *Controller) UpdateContact(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	vars := mux.Vars(r)
	id := vars["contact_id"]

	var body map[string]interface{}
	dcErr := json.NewDecoder(r.Body).Decode(&body)
	if dcErr != nil {
		Error(ctx, w, "Failed to decode request body", dcErr, http.StatusBadRequest)
		return
	}

	model := models.Contact{}
	record, err := model.Update(ctx, c.DB, id, body)
	if err != nil {
		if err.Err == gorm.ErrRecordNotFound {
			Error(ctx, w, err.Message, err.Err, http.StatusNotFound)
			return
		}
		Error(ctx, w, err.Message, err.Err, http.StatusBadRequest)
		return
	}

	expCacheErr := ExpireCache(ctx, c.Cache, c.Config.CacheSettings.Contact, id)
	if expCacheErr != nil {
		Error(ctx, w, expCacheErr.Message, expCacheErr.Err, http.StatusInternalServerError)
		return
	}

	expCacheErr = ExpireCache(ctx, c.Cache, c.Config.CacheSettings.GuildFeed, r.Header.Get("Guild"))
	if expCacheErr != nil {
		Error(ctx, w, expCacheErr.Message, expCacheErr.Err, http.StatusInternalServerError)
		return
	}

	Response(ctx, w, viewmodels.UpdateContactResponse{
		Message: "Updated record",
		Contact: *record,
	}, http.StatusOK)
}

// DeleteContact route
func (c *Controller) DeleteContact(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	vars := mux.Vars(r)
	id := vars["contact_id"]

	model := models.Contact{}
	count, err := model.Delete(ctx, c.DB, id)
	if err != nil {
		if err.Err == gorm.ErrRecordNotFound {
			Error(ctx, w, err.Message, err.Err, http.StatusNotFound)
			return
		}
		Error(ctx, w, err.Message, err.Err, http.StatusBadRequest)
		return
	}

	if count == 0 {
		Error(ctx, w, "No records to delete", errors.New("No records to delete"), http.StatusBadRequest)
		return
	}

	expCacheErr := ExpireCache(ctx, c.Cache, c.Config.CacheSettings.Contact, id)
	if expCacheErr != nil {
		Error(ctx, w, expCacheErr.Message, expCacheErr.Err, http.StatusInternalServerError)
		return
	}

	expCacheErr = ExpireCache(ctx, c.Cache, c.Config.CacheSettings.GuildFeed, r.Header.Get("Guild"))
	if expCacheErr != nil {
		Error(ctx, w, expCacheErr.Message, expCacheErr.Err, http.StatusInternalServerError)
		return
	}

	Response(ctx, w, viewmodels.DeleteContactResponse{
		Message: "Deleted records",
		Count:   count,
	}, http.StatusOK)
}
