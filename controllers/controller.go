package controllers

import (
	"context"
	"encoding/json"
	"net/http"
	"runtime/debug"

	"gitlab.com/BIC_Dev/guild-config-service/configs"
	"gitlab.com/BIC_Dev/guild-config-service/utils/cache"
	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// Controller struct
type Controller struct {
	Config              *configs.Config
	Cache               *cache.Cache
	DB                  *gorm.DB
	NitradoServiceToken string
}

// Response sends a response to the client
func Response(ctx context.Context, w http.ResponseWriter, response interface{}, status int) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	err := json.NewEncoder(w).Encode(response)

	if err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err))
		logger := logging.Logger(ctx)
		logger.Error("error_log")
	}
}

// Error sends error response to the client
func Error(ctx context.Context, w http.ResponseWriter, message string, err error, status int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	encErr := json.NewEncoder(w).Encode(viewmodels.ErrorResponse{
		Message: message,
		Error:   err.Error(),
	})

	if encErr != nil {
		encCtx := logging.AddValues(ctx, zap.NamedError("error", encErr))
		logger := logging.Logger(encCtx)
		logger.Error("error_log")
	}

	ctx = logging.AddValues(ctx,
		zap.NamedError("error", err),
		zap.String("error_message", message),
	)

	if status >= 500 {
		ctx = logging.AddValues(ctx, zap.String("trace", string(debug.Stack())))
	}

	logger := logging.Logger(ctx)
	logger.Error("error_log")
}

// GetCache func
func GetCache(ctx context.Context, ca *cache.Cache, settings configs.CacheSetting, id interface{}, output interface{}) *cache.CacheError {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if !settings.Enabled {
		return nil
	}

	cacheKey := cache.GenerateKey(settings.Base, id)
	cacheErr := ca.GetStruct(ctx, cacheKey, &output)
	return cacheErr
}

// SetCache func
func SetCache(ctx context.Context, ca *cache.Cache, settings configs.CacheSetting, id interface{}, input interface{}) *cache.CacheError {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if !settings.Enabled {
		return nil
	}

	cacheKey := cache.GenerateKey(settings.Base, id)
	cacheErr := ca.SetStruct(ctx, cacheKey, &input, settings.TTL)
	return cacheErr
}

// ExpireCache func
func ExpireCache(ctx context.Context, ca *cache.Cache, settings configs.CacheSetting, id interface{}) *cache.CacheError {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if !settings.Enabled {
		return nil
	}

	cacheKey := cache.GenerateKey(settings.Base, id)
	cacheErr := ca.Expire(ctx, cacheKey)
	return cacheErr
}
