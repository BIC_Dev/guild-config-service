package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
	"gitlab.com/BIC_Dev/nitrado-service-v3-client/nsclient"
	"gitlab.com/BIC_Dev/nitrado-service-v3-client/nsclient/services"
	"gitlab.com/BIC_Dev/nitrado-service-v3-client/nsmodels"

	"go.uber.org/zap"
	"gorm.io/gorm"
)

/*
---MODELS MUST BE SET UP BEFORE---
GuildService
NitradoToken
ServerType

---MODELS GENERATED---
Setup
Contact
GuildServiceContact
Guild
Server
BoostSetting
*/

/*
---Initial Setup---
1. Get/parse request body
2. Lookup Guild Service
	- IF exists:
		- IF NOT enabled, ERROR
3. Lookup contact by ID
	- IF exists:
		- IF details changed:
			a. UPDATE details in DB
			b. EXPIRE CACHE for Contact
	- IF NOT exists, create contact in DB
4. INSERT Setup in DB
5. Lookup Guild Service Contact by Guild Service ID and Contact ID
	- IF NOT exists, INSERT into DB
6. Lookup Guild by ID
	- IF exists:
		- IF enabled:
			- IF details changed:
				a. UPDATE details in DB
				b. EXPIRE CACHE for Guild
		- IF NOT enabled, ERROR
	- IF NOT exists, INSERT into DB
7. Lookup Nitrado Tokens by Guild ID
	- IF exists:
		IF NOT enabled, ERROR
	- IF NOT exists, ERROR
8. Lookup all supported Server Types
9. Lookup existing servers by Guild ID

---Nitrado Data---
FOR EACH TOKEN:
	1. Lookup Nitrado Services using NSv2 client
		- IF error, SKIP
	2. Check Nitrado Service status
		- IF NOT active, SKIP
	3. Check Nitrado Service type
		- IF NOT supported type, SKIP
	4. Check if Nitrado Service already exists in DB
		- IF exists, SKIP
	5. Lookup Nitrado Boost Setting using NSv2 client
		- IF error, SKIP
		- IF exists:
			a. INSERT into DB
			b. Add Boost Setting ID to Server model
	6. INSERT Server into DB

--- Finish Setup---
1. EXPIRE CACHE for Guild Feed
2. SUCCESS response
*/

// CreateNitradoSetup route
func (c *Controller) CreateNitradoSetup(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	// Decode request body
	var body viewmodels.CreateNitradoSetupRequest
	dcErr := json.NewDecoder(r.Body).Decode(&body)
	if dcErr != nil {
		Error(ctx, w, "Failed to decode request body", dcErr, http.StatusBadRequest)
		return
	}

	// Check if guild service exists and is enabled
	guildServiceModel := models.GuildService{}
	guildService, gsErr := guildServiceModel.FindByGuildAndName(ctx, c.DB, body.GuildID, body.GuildServiceName)
	if gsErr != nil {
		Error(ctx, w, gsErr.Message, gsErr.Err, http.StatusUnauthorized)
		return
	} else if !guildService.Enabled {
		Error(ctx, w, "You are not authorized to setup this bot. Please contact BIC for more details", fmt.Errorf("not authorized"), http.StatusUnauthorized)
		return
	}

	// Insert or update existing contact
	contactModel := models.Contact{}
	contact, contErr := contactModel.FindByID(ctx, c.DB, body.ContactID)
	if contErr != nil {
		if contErr.Err != gorm.ErrRecordNotFound {
			Error(ctx, w, contErr.Message, contErr.Err, http.StatusInternalServerError)
			return
		}

		contactModel = models.Contact{
			ID:      body.ContactID,
			Name:    body.ContactName,
			Enabled: true,
		}
		newContact, ncErr := contactModel.Create(ctx, c.DB)
		if ncErr != nil {
			Error(ctx, w, ncErr.Message, ncErr.Err, http.StatusInternalServerError)
			return
		}
		contact = newContact
	} else if contact.Name != body.ContactName {
		upContact, ucErr := contactModel.Update(ctx, c.DB, contact.ID, map[string]interface{}{
			"name": body.ContactName,
		})
		if ucErr != nil {
			Error(ctx, w, ucErr.Message, ucErr.Err, http.StatusInternalServerError)
			return
		}
		expCacheErr := ExpireCache(ctx, c.Cache, c.Config.CacheSettings.Contact, contact.ID)
		if expCacheErr != nil {
			Error(ctx, w, expCacheErr.Message, expCacheErr.Err, http.StatusInternalServerError)
			return
		}
		contact = upContact
	}

	// Insert setup into DB
	setupModel := models.Setup{
		GuildID:        &body.GuildID,
		ContactID:      &body.ContactID,
		GuildServiceID: &guildService.ID,
	}
	setup, setErr := setupModel.Create(ctx, c.DB)
	if setErr != nil {
		Error(ctx, w, setErr.Message, setErr.Err, http.StatusInternalServerError)
		return
	}

	// Lookup guild service contact or create new one if not exists
	guildServiceContactModel := models.GuildServiceContact{}
	_, gscErr := guildServiceContactModel.FindByGuildServiceAndContact(ctx, c.DB, guildService.ID, contact.ID)
	if gscErr != nil {
		if gscErr.Err != gorm.ErrRecordNotFound {
			Error(ctx, w, gscErr.Message, gscErr.Err, http.StatusInternalServerError)
			return
		}

		guildServiceContactModel = models.GuildServiceContact{
			GuildServiceID: &guildService.ID,
			ContactID:      &contact.ID,
		}
		_, ngscErr := guildServiceContactModel.Create(ctx, c.DB)
		if ngscErr != nil {
			Error(ctx, w, ngscErr.Message, ngscErr.Err, http.StatusInternalServerError)
			return
		}
	}

	// Lookup guild or create new one if not exists
	guildModel := models.Guild{}
	guild, gErr := guildModel.FindByID(ctx, c.DB, body.GuildID)
	if gErr != nil {
		if gErr.Err != gorm.ErrRecordNotFound {
			Error(ctx, w, gErr.Message, gErr.Err, http.StatusInternalServerError)
			return
		}

		guildModel = models.Guild{
			ID:      body.GuildID,
			Name:    body.GuildName,
			Enabled: true,
		}
		newGuild, ngErr := guildModel.Create(ctx, c.DB)
		if ngErr != nil {
			Error(ctx, w, ngErr.Message, ngErr.Err, http.StatusInternalServerError)
			return
		}
		guild = newGuild
	} else if guild.Name != body.GuildName {
		upGuild, ugErr := guildModel.Update(ctx, c.DB, guild.ID, map[string]interface{}{
			"Name": body.GuildName,
		})
		if ugErr != nil {
			Error(ctx, w, ugErr.Message, ugErr.Err, http.StatusInternalServerError)
			return
		}
		expCacheErr := ExpireCache(ctx, c.Cache, c.Config.CacheSettings.Guild, guild.ID)
		if expCacheErr != nil {
			Error(ctx, w, expCacheErr.Message, expCacheErr.Err, http.StatusInternalServerError)
			return
		}
		guild = upGuild
	}

	// Lookup Nitrado Tokens
	nitradoTokenModel := models.NitradoToken{}
	nitradoTokens, ntErr := nitradoTokenModel.FindByGuild(ctx, c.DB, guild.ID)
	if ntErr != nil {
		if ntErr.Err == gorm.ErrRecordNotFound {
			Error(ctx, w, "No Nitrado Long-life Tokens exist. Please contact BIC to add them.", ntErr.Err, http.StatusUnauthorized)
			return
		}

		Error(ctx, w, ntErr.Message, ntErr.Err, http.StatusInternalServerError)
		return
	}

	// Lookup Server Types
	serverTypesModel := models.ServerType{}
	serverTypes, stErr := serverTypesModel.FindAll(ctx, c.DB)
	if stErr != nil {
		Error(ctx, w, stErr.Message, stErr.Err, http.StatusInternalServerError)
		return
	}

	// Lookup Servers
	existingServersModel := models.Server{}
	existingServers, esErr := existingServersModel.FindByGuild(ctx, c.DB, guild.ID)
	if esErr != nil {
		if esErr.Err != gorm.ErrRecordNotFound {
			Error(ctx, w, esErr.Message, esErr.Err, http.StatusInternalServerError)
			return
		}
	}

	// Create Nitrado Service V3 Client
	nsctransport := nsclient.TransportConfig{
		Host:     c.Config.NitradoService.Host,
		BasePath: c.Config.NitradoService.BasePath,
		Schemes:  nil,
	}

	client := nsclient.NewHTTPClientWithConfig(strfmt.Default, &nsctransport)

	// Lookup new servers and boost codes from Nitrado and insert into DB
	var newServers []models.Server
	for _, token := range *nitradoTokens {
		if !token.Enabled {
			continue
		}

		clientAuth := NitradoServiceV3Token(c.NitradoServiceToken)

		servicesParams := services.NewGetServicesParamsWithTimeout(30 * time.Second)
		servicesParams.SetBody(services.GetServicesBody{
			Tokens: []*nsmodels.Token{
				{
					ID:    &token.Token,
					Value: &token.TokenValue,
				},
			},
		})

		services, svErr := client.Services.GetServices(servicesParams, clientAuth)
		if svErr != nil {
			newCtx := logging.AddValues(ctx, zap.NamedError("error", svErr), zap.String("error_message", "Failed to retrieve Nitrado services"))
			logger := logging.Logger(newCtx)
			logger.Error("error_log")
			continue
		}

		if services.Payload.AccountServices == nil || len(services.Payload.AccountServices) == 0 {
			continue
		}

		for _, server := range services.Payload.AccountServices[0].Services {
			if strings.ToLower(server.Status) != "active" && strings.ToLower(server.Status) != "stopped" && strings.ToLower(server.Status) != "restarting" {
				continue
			}

			isExistingServer := false
			for _, exServer := range *existingServers {
				if int(server.ID) == exServer.NitradoID {
					isExistingServer = true
					break
				}
			}

			if isExistingServer {
				continue
			}

			var serverType *models.ServerType
			for _, st := range *serverTypes {
				if st.ID == strings.ToLower(server.Details.FolderShort) {
					serverType = &st
					break
				}
			}

			if serverType == nil {
				continue
			}

			// nitradoBoostSetting, nbsErr := client.GetBoostSettings(token.Token, server.ID)
			// if nbsErr != nil {
			// 	newCtx := logging.AddValues(ctx, zap.NamedError("error", errors.New(nbsErr.Error())), zap.String("error_message", nbsErr.Message()))
			// 	logger := logging.Logger(newCtx)
			// 	logger.Error("error_log")
			// }

			// var boostSetting *models.BoostSetting
			// if nitradoBoostSetting.Error == "" {
			// 	boostSettingsModel := models.BoostSetting{
			// 		Code:           nitradoBoostSetting.BoostSettings.Code,
			// 		Message:        nitradoBoostSetting.BoostSettings.Message,
			// 		WelcomeMessage: nitradoBoostSetting.BoostSettings.WelcomeMessage,
			// 		BoostEnabled:   nitradoBoostSetting.BoostSettings.Enabled,
			// 		Enabled:        true,
			// 	}
			// 	newBoostSetting, newbsErr := boostSettingsModel.Create(ctx, c.DB)
			// 	if newbsErr != nil {
			// 		Error(ctx, w, newbsErr.Message, newbsErr.Err, http.StatusInternalServerError)
			// 		return
			// 	}
			// 	boostSetting = newBoostSetting
			// }

			newServerModel := models.Server{
				NitradoID:      int(server.ID),
				Name:           server.Details.Name,
				ServerTypeID:   &serverType.ID,
				GuildID:        &guild.ID,
				NitradoTokenID: &token.ID,
				Enabled:        true,
			}

			// if boostSetting != nil {
			// 	newServerModel.BoostSettingID = &boostSetting.ID
			// }

			newServer, nservErr := newServerModel.Create(ctx, c.DB)
			if nservErr != nil {
				Error(ctx, w, nservErr.Message, nservErr.Err, http.StatusInternalServerError)
				return
			}
			newServers = append(newServers, *newServer)
		}
	}

	// EXPIRE Guild Feed cache
	expCacheErr := ExpireCache(ctx, c.Cache, c.Config.CacheSettings.GuildFeed, guild.ID)
	if expCacheErr != nil {
		Error(ctx, w, expCacheErr.Message, expCacheErr.Err, http.StatusInternalServerError)
		return
	}

	// SUCCESS response
	Response(ctx, w, viewmodels.CreateNitradoSetupResponse{
		Message:       "Successfull status setup",
		NitradoTokens: *nitradoTokens,
		Servers:       newServers,
		Contact:       *contact,
		Setup:         *setup,
	}, http.StatusCreated)
}

// NitradoServiceV3Token func
func NitradoServiceV3Token(token string) runtime.ClientAuthInfoWriter {
	return runtime.ClientAuthInfoWriterFunc(func(r runtime.ClientRequest, _ strfmt.Registry) error {
		return r.SetHeaderParam("Service-Token", token)
	})
}
