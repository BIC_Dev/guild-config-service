package controllers

// // CreateNSMSetup route
// func (c *Controller) CreateNSMSetup(w http.ResponseWriter, r *http.Request) {
// 	ctx := r.Context()
// 	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

// 	var body models.Setup
// 	dcErr := json.NewDecoder(r.Body).Decode(&body)
// 	if dcErr != nil {
// 		Error(ctx, w, "Failed to decode request body", dcErr, http.StatusBadRequest)
// 		return
// 	}

// 	record, err := body.Create(ctx, c.DB)
// 	if err != nil {
// 		Error(ctx, w, err.Message, err.Err, http.StatusBadRequest)
// 		return
// 	}

// 	Response(ctx, w, viewmodels.CreateSetupResponse{
// 		Message: "Created record",
// 		Setup:   *record,
// 	}, http.StatusCreated)
// }

// // DeleteNSMSetup route
// func (c *Controller) DeleteNSMSetup(w http.ResponseWriter, r *http.Request) {
// 	ctx := r.Context()
// 	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

// 	vars := mux.Vars(r)
// 	id := vars["id"]

// 	model := models.Setup{}
// 	count, err := model.Delete(ctx, c.DB, id)
// 	if err != nil {
// 		Error(ctx, w, err.Message, err.Err, http.StatusBadRequest)
// 		return
// 	}

// 	if count == 0 {
// 		Error(ctx, w, "No records to delete", errors.New("No records to delete"), http.StatusBadRequest)
// 		return
// 	}

// 	Response(ctx, w, viewmodels.DeleteSetupResponse{
// 		Message: "Deleted records",
// 		Count:   count,
// 	}, http.StatusOK)
// }
