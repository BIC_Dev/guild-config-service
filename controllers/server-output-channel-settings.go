package controllers

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// GetAllServerOutputChannelSettings route
func (c *Controller) GetAllServerOutputChannelSettings(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	model := models.ServerOutputChannelSetting{}
	records, err := model.FindAll(ctx, c.DB)
	if err != nil {
		Error(ctx, w, err.Message, err.Err, http.StatusBadRequest)
		return
	}

	Response(ctx, w, viewmodels.GetAllServerOutputChannelSettingsResponse{
		Message:                     "Found records",
		ServerOutputChannelSettings: *records,
	}, http.StatusOK)
}

// GetServerOutputChannelSettingByID route
func (c *Controller) GetServerOutputChannelSettingByID(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	vars := mux.Vars(r)
	id := vars["server_output_channel_setting_id"]

	idInt, cErr := strconv.Atoi(id)
	if cErr != nil {
		Error(ctx, w, "Failed to convert id to int", cErr, http.StatusBadRequest)
		return
	}

	var modelCache *models.ServerOutputChannelSetting
	getCacheErr := GetCache(ctx, c.Cache, c.Config.CacheSettings.ServerOutputChannelSetting, id, &modelCache)
	if getCacheErr != nil {
		Error(ctx, w, getCacheErr.Message, getCacheErr.Err, http.StatusInternalServerError)
		return
	}

	if modelCache != nil {
		w.Header().Set("X-Cache", "HIT")
		Response(ctx, w, viewmodels.GetServerOutputChannelSettingByIDResponse{
			Message:                    "Found record",
			ServerOutputChannelSetting: *modelCache,
		}, http.StatusOK)
		return
	}

	model := models.ServerOutputChannelSetting{}
	record, err := model.FindByID(ctx, c.DB, uint(idInt))
	if err != nil {
		if err.Err == gorm.ErrRecordNotFound {
			Error(ctx, w, err.Message, err.Err, http.StatusNotFound)
			return
		}
		Error(ctx, w, err.Message, err.Err, http.StatusBadRequest)
		return
	}

	setCacheErr := SetCache(ctx, c.Cache, c.Config.CacheSettings.ServerOutputChannelSetting, id, record)
	if setCacheErr != nil {
		Error(ctx, w, setCacheErr.Message, setCacheErr.Err, http.StatusInternalServerError)
		return
	}

	w.Header().Set("X-Cache", "MISS")
	Response(ctx, w, viewmodels.GetServerOutputChannelSettingByIDResponse{
		Message:                    "Found record",
		ServerOutputChannelSetting: *record,
	}, http.StatusOK)
}

// CreateServerOutputChannelSetting route
func (c *Controller) CreateServerOutputChannelSetting(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	var body models.ServerOutputChannelSetting
	dcErr := json.NewDecoder(r.Body).Decode(&body)
	if dcErr != nil {
		Error(ctx, w, "Failed to decode request body", dcErr, http.StatusBadRequest)
		return
	}

	record, err := body.Create(ctx, c.DB)
	if err != nil {
		Error(ctx, w, err.Message, err.Err, http.StatusBadRequest)
		return
	}

	expCacheErr := ExpireCache(ctx, c.Cache, c.Config.CacheSettings.GuildFeed, r.Header.Get("Guild"))
	if expCacheErr != nil {
		Error(ctx, w, expCacheErr.Message, expCacheErr.Err, http.StatusInternalServerError)
		return
	}

	Response(ctx, w, viewmodels.CreateServerOutputChannelSettingResponse{
		Message:                    "Created record",
		ServerOutputChannelSetting: *record,
	}, http.StatusCreated)
}

// UpdateServerOutputChannelSetting route
func (c *Controller) UpdateServerOutputChannelSetting(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	vars := mux.Vars(r)
	id := vars["server_output_channel_setting_id"]
	idInt, cErr := strconv.Atoi(id)
	if cErr != nil {
		Error(ctx, w, "Failed to convert id to int", cErr, http.StatusBadRequest)
		return
	}

	var body map[string]interface{}
	dcErr := json.NewDecoder(r.Body).Decode(&body)
	if dcErr != nil {
		Error(ctx, w, "Failed to decode request body", dcErr, http.StatusBadRequest)
		return
	}

	model := models.ServerOutputChannelSetting{}
	record, err := model.Update(ctx, c.DB, uint(idInt), body)
	if err != nil {
		if err.Err == gorm.ErrRecordNotFound {
			Error(ctx, w, err.Message, err.Err, http.StatusNotFound)
			return
		}
		Error(ctx, w, err.Message, err.Err, http.StatusBadRequest)
		return
	}

	expCacheErr := ExpireCache(ctx, c.Cache, c.Config.CacheSettings.ServerOutputChannelSetting, id)
	if expCacheErr != nil {
		Error(ctx, w, expCacheErr.Message, expCacheErr.Err, http.StatusInternalServerError)
		return
	}

	expCacheErr = ExpireCache(ctx, c.Cache, c.Config.CacheSettings.GuildFeed, r.Header.Get("Guild"))
	if expCacheErr != nil {
		Error(ctx, w, expCacheErr.Message, expCacheErr.Err, http.StatusInternalServerError)
		return
	}

	Response(ctx, w, viewmodels.UpdateServerOutputChannelSettingResponse{
		Message:                    "Updated record",
		ServerOutputChannelSetting: *record,
	}, http.StatusOK)
}

// DeleteServerOutputChannelSetting route
func (c *Controller) DeleteServerOutputChannelSetting(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	vars := mux.Vars(r)
	id := vars["server_output_channel_setting_id"]
	idInt, cErr := strconv.Atoi(id)
	if cErr != nil {
		Error(ctx, w, "Failed to convert id to int", cErr, http.StatusBadRequest)
		return
	}

	model := models.ServerOutputChannelSetting{}
	count, err := model.Delete(ctx, c.DB, uint(idInt))
	if err != nil {
		if err.Err == gorm.ErrRecordNotFound {
			Error(ctx, w, err.Message, err.Err, http.StatusNotFound)
			return
		}
		Error(ctx, w, err.Message, err.Err, http.StatusBadRequest)
		return
	}

	if count == 0 {
		Error(ctx, w, "No records to delete", errors.New("No records to delete"), http.StatusBadRequest)
		return
	}

	expCacheErr := ExpireCache(ctx, c.Cache, c.Config.CacheSettings.ServerOutputChannelSetting, id)
	if expCacheErr != nil {
		Error(ctx, w, expCacheErr.Message, expCacheErr.Err, http.StatusInternalServerError)
		return
	}

	expCacheErr = ExpireCache(ctx, c.Cache, c.Config.CacheSettings.GuildFeed, r.Header.Get("Guild"))
	if expCacheErr != nil {
		Error(ctx, w, expCacheErr.Message, expCacheErr.Err, http.StatusInternalServerError)
		return
	}

	Response(ctx, w, viewmodels.DeleteServerOutputChannelSettingResponse{
		Message: "Deleted records",
		Count:   count,
	}, http.StatusOK)
}
