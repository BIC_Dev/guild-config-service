package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /ark-tribe-members ArkTribeMembers getAllArkTribeMembers
// Get all servers
// responses:
//   200: getAllArkTribeMembersResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get all servers response body
// swagger:response getAllArkTribeMembersResponse
type getAllArkTribeMembersResponse struct {
	// in:body
	Body viewmodels.GetAllArkTribeMembersResponse
}

// swagger:route GET /ark-tribe-members/{ark_tribe_member_id} ArkTribeMembers getArkTribeMemberByID
// Get server by ID
// responses:
//   200: getArkTribeMemberByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get server by ID response body
// swagger:response getArkTribeMemberByIDResponse
type getArkTribeMemberByIDResponse struct {
	// in:body
	Body viewmodels.GetArkTribeMemberByIDResponse
}

// swagger:route POST /ark-tribe-members ArkTribeMembers createArkTribeMember
// Create a server
// responses:
//   201: createArkTribeMemberResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create server request
// swagger:parameters createArkTribeMember
type createArkTribeMemberRequest struct {
	// in:body
	Body models.ArkTribeMember
}

// Create server response
// swagger:response createArkTribeMemberResponse
type createArkTribeMemberResponse struct {
	// in:body
	Body viewmodels.CreateArkTribeMemberResponse
}

// swagger:route PUT /ark-tribe-members/{ark_tribe_member_id} ArkTribeMembers updateArkTribeMember
// Update a server
// responses:
//   200: updateArkTribeMemberResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create server request
// swagger:parameters updateArkTribeMember
type updateArkTribeMemberRequest struct {
	// in:body
	Body viewmodels.UpdateArkTribeMemberRequest
}

// Update server response
// swagger:response updateArkTribeMemberResponse
type updateArkTribeMemberResponse struct {
	// in:body
	Body viewmodels.UpdateArkTribeMemberResponse
}

// swagger:route DELETE /ark-tribe-members/{ark_tribe_member_id} ArkTribeMembers deleteArkTribeMember
// Deletes a server
// responses:
//   200: deleteArkTribeMemberResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Delete server response
// swagger:response deleteArkTribeMemberResponse
type deleteArkTribeMemberResponse struct {
	// in: body
	Body viewmodels.DeleteArkTribeMemberResponse
}

// swagger:parameters getArkTribeMemberByID
// swagger:parameters updateArkTribeMember
// swagger:parameters deleteArkTribeMember
type arkTribeMemberPathParams struct {
	// ArkTribeMember ID
	//
	// in: path
	// required: true
	ArkTribeMemberID int `json:"ark_tribe_member_id"`
}
