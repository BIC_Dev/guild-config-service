package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /ark-tribes ArkTribes getAllArkTribes
// Get all servers
// responses:
//   200: getAllArkTribesResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get all servers response body
// swagger:response getAllArkTribesResponse
type getAllArkTribesResponse struct {
	// in:body
	Body viewmodels.GetAllArkTribesResponse
}

// swagger:route GET /ark-tribes/{ark_tribe_id} ArkTribes getArkTribeByID
// Get server by ID
// responses:
//   200: getArkTribeByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get server by ID response body
// swagger:response getArkTribeByIDResponse
type getArkTribeByIDResponse struct {
	// in:body
	Body viewmodels.GetArkTribeByIDResponse
}

// swagger:route POST /ark-tribes ArkTribes createArkTribe
// Create a server
// responses:
//   201: createArkTribeResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create server request
// swagger:parameters createArkTribe
type createArkTribeRequest struct {
	// in:body
	Body models.ArkTribe
}

// Create server response
// swagger:response createArkTribeResponse
type createArkTribeResponse struct {
	// in:body
	Body viewmodels.CreateArkTribeResponse
}

// swagger:route PUT /ark-tribes/{ark_tribe_id} ArkTribes updateArkTribe
// Update a server
// responses:
//   200: updateArkTribeResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create server request
// swagger:parameters updateArkTribe
type updateArkTribeRequest struct {
	// in:body
	Body viewmodels.UpdateArkTribeRequest
}

// Update server response
// swagger:response updateArkTribeResponse
type updateArkTribeResponse struct {
	// in:body
	Body viewmodels.UpdateArkTribeResponse
}

// swagger:route DELETE /ark-tribes/{ark_tribe_id} ArkTribes deleteArkTribe
// Deletes a server
// responses:
//   200: deleteArkTribeResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Delete server response
// swagger:response deleteArkTribeResponse
type deleteArkTribeResponse struct {
	// in: body
	Body viewmodels.DeleteArkTribeResponse
}

// swagger:parameters getArkTribeByID
// swagger:parameters updateArkTribe
// swagger:parameters deleteArkTribe
type arkTribePathParams struct {
	// ArkTribe ID
	//
	// in: path
	// required: true
	ArkTribeID int `json:"ark_tribe_id"`
}
