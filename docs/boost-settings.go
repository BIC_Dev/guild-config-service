package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /boost-settings BoostSettings getAllBoostSettings
// Get all boost settings
// responses:
//   200: getAllBoostSettingsResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get all boost settings response body
// swagger:response getAllBoostSettingsResponse
type getAllBoostSettingsResponse struct {
	// in:body
	Body viewmodels.GetAllBoostSettingsResponse
}

// swagger:route GET /boost-settings/{boost_setting_id} BoostSettings getBoostSettingByID
// Get boost setting by ID
// responses:
//   200: getBoostSettingByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get boost setting by ID response body
// swagger:response getBoostSettingByIDResponse
type getBoostSettingByIDResponse struct {
	// in:body
	Body viewmodels.GetBoostSettingByIDResponse
}

// swagger:route POST /boost-settings BoostSettings createBoostSetting
// Create a boost setting
// responses:
//   201: createBoostSettingResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create boost setting request
// swagger:parameters createBoostSetting
type createBoostSettingRequest struct {
	// in:body
	Body models.BoostSetting
}

// Create boost setting response
// swagger:response createBoostSettingResponse
type createBoostSettingResponse struct {
	// in:body
	Body viewmodels.CreateBoostSettingResponse
}

// swagger:route PUT /boost-settings/{boost_setting_id} BoostSettings updateBoostSetting
// Update a boost setting
// responses:
//   200: updateBoostSettingResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create boost setting request
// swagger:parameters updateBoostSetting
type updateBoostSettingRequest struct {
	// in:body
	Body viewmodels.UpdateBoostSettingRequest
}

// Update boost setting response
// swagger:response updateBoostSettingResponse
type updateBoostSettingResponse struct {
	// in:body
	Body viewmodels.UpdateBoostSettingResponse
}

// swagger:route DELETE /boost-settings/{boost_setting_id} BoostSettings deleteBoostSetting
// Deletes a boost setting
// responses:
//   200: deleteBoostSettingResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Delete boost setting response
// swagger:response deleteBoostSettingResponse
type deleteBoostSettingResponse struct {
	// in: body
	Body viewmodels.DeleteBoostSettingResponse
}

// swagger:parameters getBoostSettingByID
// swagger:parameters updateBoostSetting
// swagger:parameters deleteBoostSetting
type boostSettingID struct {
	// Boost Setting ID
	//
	// in: path
	// required: true
	BoostSettingID int `json:"boost_setting_id"`
}
