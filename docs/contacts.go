package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /contacts Contacts getAllContacts
// Get all contacts
// responses:
//   200: getAllContactsResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get all contacts response body
// swagger:response getAllContactsResponse
type getAllContactsResponse struct {
	// in:body
	Body viewmodels.GetAllContactsResponse
}

// swagger:route GET /contacts/{contact_id} Contacts getContactByID
// Get contact by ID
// responses:
//   200: getContactByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get contact by ID response body
// swagger:response getContactByIDResponse
type getContactByIDResponse struct {
	// in:body
	Body viewmodels.GetContactByIDResponse
}

// swagger:route POST /contacts Contacts createContact
// Create a contact
// responses:
//   201: createContactResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create contact request
// swagger:parameters createContact
type createContactRequest struct {
	// in:body
	Body models.Contact
}

// Create contact response
// swagger:response createContactResponse
type createContactResponse struct {
	// in:body
	Body viewmodels.CreateContactResponse
}

// swagger:route PUT /contacts/{contact_id} Contacts updateContact
// Update a contact
// responses:
//   200: updateContactResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create contact request
// swagger:parameters updateContact
type updateContactRequest struct {
	// in:body
	Body viewmodels.UpdateContactRequest
}

// Update contact response
// swagger:response updateContactResponse
type updateContactResponse struct {
	// in:body
	Body viewmodels.UpdateContactResponse
}

// swagger:route DELETE /contacts/{contact_id} Contacts deleteContact
// Deletes a contact
// responses:
//   200: deleteContactResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Delete contact response
// swagger:response deleteContactResponse
type deleteContactResponse struct {
	// in: body
	Body viewmodels.DeleteContactResponse
}

// swagger:parameters getContactByID
// swagger:parameters updateContact
// swagger:parameters deleteContact
type contactID struct {
	// Contact ID
	//
	// in: path
	// required: true
	ContactID string `json:"contact_id"`
}
