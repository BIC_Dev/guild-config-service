package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /guild-feeds/{guild_id} GuildFeeds getGuildFeedByID
// Get guild by ID
// responses:
//   200: getGuildFeedByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get guild feed by ID response body
// swagger:response getGuildFeedByIDResponse
type getGuildFeedByIDResponse struct {
	// in:body
	Body viewmodels.GetGuildFeedByIDResponse
}

// swagger:parameters getGuildFeedByID
type guildFeedID struct {
	// Guild ID
	//
	// in: path
	// required: true
	GuildID string `json:"guild_id"`
}
