package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /guild-output-channels GuildOutputChannels getAllGuildOutputChannels
// Get all server output channels
// responses:
//   200: getAllGuildOutputChannelsResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get all server output channels response body
// swagger:response getAllGuildOutputChannelsResponse
type getAllGuildOutputChannelsResponse struct {
	// in:body
	Body viewmodels.GetAllGuildOutputChannelsResponse
}

// swagger:route GET /guild-output-channels/{guild_output_channel_id} GuildOutputChannels getGuildOutputChannelByID
// Get server output channel by ID
// responses:
//   200: getGuildOutputChannelByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get server output channel by ID response body
// swagger:response getGuildOutputChannelByIDResponse
type getGuildOutputChannelByIDResponse struct {
	// in:body
	Body viewmodels.GetGuildOutputChannelByIDResponse
}

// swagger:route POST /guild-output-channels GuildOutputChannels createGuildOutputChannel
// Create a server output channel
// responses:
//   201: createGuildOutputChannelResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create server output channel request
// swagger:parameters createGuildOutputChannel
type createGuildOutputChannelRequest struct {
	// in:body
	Body models.GuildOutputChannel
}

// Create server output channel response
// swagger:response createGuildOutputChannelResponse
type createGuildOutputChannelResponse struct {
	// in:body
	Body viewmodels.CreateGuildOutputChannelResponse
}

// swagger:route PUT /guild-output-channels/{guild_output_channel_id} GuildOutputChannels updateGuildOutputChannel
// Update a server output channel
// responses:
//   200: updateGuildOutputChannelResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create server output channel request
// swagger:parameters updateGuildOutputChannel
type updateGuildOutputChannelRequest struct {
	// in:body
	Body viewmodels.UpdateGuildOutputChannelRequest
}

// Update server output channel response
// swagger:response updateGuildOutputChannelResponse
type updateGuildOutputChannelResponse struct {
	// in:body
	Body viewmodels.UpdateGuildOutputChannelResponse
}

// swagger:route DELETE /guild-output-channels/{guild_output_channel_id} GuildOutputChannels deleteGuildOutputChannel
// Deletes a server output channel
// responses:
//   200: deleteGuildOutputChannelResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Delete server output channel response
// swagger:response deleteGuildOutputChannelResponse
type deleteGuildOutputChannelResponse struct {
	// in: body
	Body viewmodels.DeleteGuildOutputChannelResponse
}

// swagger:parameters getGuildOutputChannelByID
// swagger:parameters updateGuildOutputChannel
// swagger:parameters deleteGuildOutputChannel
type guildOutputChannelID struct {
	// GuildOutputChannel ID
	//
	// in: path
	// required: true
	GuildOutputChannelID int `json:"guild_output_channel_id"`
}
