package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /guild-service-contacts GuildServiceContacts getAllGuildServiceContacts
// Get all guild service contacts
// responses:
//   200: getAllGuildServiceContactsResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get all guild service contacts response body
// swagger:response getAllGuildServiceContactsResponse
type getAllGuildServiceContactsResponse struct {
	// in:body
	Body viewmodels.GetAllGuildServiceContactsResponse
}

// swagger:route GET /guild-service-contacts/{guild_service_contact_id} GuildServiceContacts getGuildServiceContactByID
// Get guild service contact by ID
// responses:
//   200: getGuildServiceContactByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get guild service contact by ID response body
// swagger:response getGuildServiceContactByIDResponse
type getGuildServiceContactByIDResponse struct {
	// in:body
	Body viewmodels.GetGuildServiceContactByIDResponse
}

// swagger:route POST /guild-service-contacts GuildServiceContacts createGuildServiceContact
// Create a guild service contact
// responses:
//   201: createGuildServiceContactResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create guild service contact request
// swagger:parameters createGuildServiceContact
type createGuildServiceContactRequest struct {
	// in:body
	Body models.GuildServiceContact
}

// Create guild service contact response
// swagger:response createGuildServiceContactResponse
type createGuildServiceContactResponse struct {
	// in:body
	Body viewmodels.CreateGuildServiceContactResponse
}

// swagger:route PUT /guild-service-contacts/{guild_service_contact_id} GuildServiceContacts updateGuildServiceContact
// Update a guild service contact
// responses:
//   200: updateGuildServiceContactResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create guild service contact request
// swagger:parameters updateGuildServiceContact
type updateGuildServiceContactRequest struct {
	// in:body
	Body viewmodels.UpdateGuildServiceContactRequest
}

// Update guild service contact response
// swagger:response updateGuildServiceContactResponse
type updateGuildServiceContactResponse struct {
	// in:body
	Body viewmodels.UpdateGuildServiceContactResponse
}

// swagger:route DELETE /guild-service-contacts/{guild_service_contact_id} GuildServiceContacts deleteGuildServiceContact
// Deletes a guild service contact
// responses:
//   200: deleteGuildServiceContactResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Delete guild service contact response
// swagger:response deleteGuildServiceContactResponse
type deleteGuildServiceContactResponse struct {
	// in: body
	Body viewmodels.DeleteGuildServiceContactResponse
}

// swagger:parameters getGuildServiceContactByID
// swagger:parameters updateGuildServiceContact
// swagger:parameters deleteGuildServiceContact
type guildServiceContactID struct {
	// Guild Service Contact ID
	//
	// in: path
	// required: true
	GuildServiceContactID int `json:"guild_service_contact_id"`
}
