package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /guild-service-permissions GuildServicePermissions getAllGuildServicePermissions
// Get all guild service permissions
// responses:
//   200: getAllGuildServicePermissionsResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get all guild service permissions response body
// swagger:response getAllGuildServicePermissionsResponse
type getAllGuildServicePermissionsResponse struct {
	// in:body
	Body viewmodels.GetAllGuildServicePermissionsResponse
}

// swagger:route GET /guild-service-permissions/{guild_service_permission_id} GuildServicePermissions getGuildServicePermissionByID
// Get guild service permission by ID
// responses:
//   200: getGuildServicePermissionByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get guild service permission by ID response body
// swagger:response getGuildServicePermissionByIDResponse
type getGuildServicePermissionByIDResponse struct {
	// in:body
	Body viewmodels.GetGuildServicePermissionByIDResponse
}

// swagger:route POST /guild-service-permissions GuildServicePermissions createGuildServicePermission
// Create a guild service permission
// responses:
//   201: createGuildServicePermissionResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create guild service permission request
// swagger:parameters createGuildServicePermission
type createGuildServicePermissionRequest struct {
	// in:body
	Body models.GuildServicePermission
}

// Create guild service permission response
// swagger:response createGuildServicePermissionResponse
type createGuildServicePermissionResponse struct {
	// in:body
	Body viewmodels.CreateGuildServicePermissionResponse
}

// swagger:route PUT /guild-service-permissions/{guild_service_permission_id} GuildServicePermissions updateGuildServicePermission
// Update a guild service permission
// responses:
//   200: updateGuildServicePermissionResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create guild service permission request
// swagger:parameters updateGuildServicePermission
type updateGuildServicePermissionRequest struct {
	// in:body
	Body viewmodels.UpdateGuildServicePermissionRequest
}

// Update guild service permission response
// swagger:response updateGuildServicePermissionResponse
type updateGuildServicePermissionResponse struct {
	// in:body
	Body viewmodels.UpdateGuildServicePermissionResponse
}

// swagger:route DELETE /guild-service-permissions/{guild_service_permission_id} GuildServicePermissions deleteGuildServicePermission
// Deletes a guild service permission
// responses:
//   200: deleteGuildServicePermissionResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Delete guild service permission response
// swagger:response deleteGuildServicePermissionResponse
type deleteGuildServicePermissionResponse struct {
	// in: body
	Body viewmodels.DeleteGuildServicePermissionResponse
}

// swagger:parameters getGuildServicePermissionByID
// swagger:parameters updateGuildServicePermission
// swagger:parameters deleteGuildServicePermission
type guildServicePermissionID struct {
	// Guild Service Permission ID
	//
	// in: path
	// required: true
	GuildServicePermissionID int `json:"guild_service_permission_id"`
}
