package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /guild-services GuildServices getAllGuildServices
// Get all guild services
// responses:
//   200: getAllGuildServicesResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get all guild services response body
// swagger:response getAllGuildServicesResponse
type getAllGuildServicesResponse struct {
	// in:body
	Body viewmodels.GetAllGuildServicesResponse
}

// swagger:route GET /guild-services/{guild_service_id} GuildServices getGuildServiceByID
// Get guild service by ID
// responses:
//   200: getGuildServiceByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get guild service by ID response body
// swagger:response getGuildServiceByIDResponse
type getGuildServiceByIDResponse struct {
	// in:body
	Body viewmodels.GetGuildServiceByIDResponse
}

// swagger:route POST /guild-services GuildServices createGuildService
// Create a guild service
// responses:
//   201: createGuildServiceResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create guild service request
// swagger:parameters createGuildService
type createGuildServiceRequest struct {
	// in:body
	Body models.GuildService
}

// Create guild service response
// swagger:response createGuildServiceResponse
type createGuildServiceResponse struct {
	// in:body
	Body viewmodels.CreateGuildServiceResponse
}

// swagger:route PUT /guild-services/{guild_service_id} GuildServices updateGuildService
// Update a guild service
// responses:
//   200: updateGuildServiceResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create guild service request
// swagger:parameters updateGuildService
type updateGuildServiceRequest struct {
	// in:body
	Body viewmodels.UpdateGuildServiceRequest
}

// Update guild service response
// swagger:response updateGuildServiceResponse
type updateGuildServiceResponse struct {
	// in:body
	Body viewmodels.UpdateGuildServiceResponse
}

// swagger:route DELETE /guild-services/{guild_service_id} GuildServices deleteGuildService
// Deletes a guild service
// responses:
//   200: deleteGuildServiceResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Delete guild service response
// swagger:response deleteGuildServiceResponse
type deleteGuildServiceResponse struct {
	// in: body
	Body viewmodels.DeleteGuildServiceResponse
}

// swagger:parameters getGuildServiceByID
// swagger:parameters updateGuildService
// swagger:parameters deleteGuildService
type guildServiceID struct {
	// Guild Service ID
	//
	// in: path
	// required: true
	GuildServiceID int `json:"guild_service_id"`
}
