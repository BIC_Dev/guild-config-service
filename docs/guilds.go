package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /guilds Guilds getAllGuilds
// Get all guilds
// responses:
//   200: getAllGuildsResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get all guilds response body
// swagger:response getAllGuildsResponse
type getAllGuildsResponse struct {
	// in:body
	Body viewmodels.GetAllGuildsResponse
}

// swagger:route GET /guilds/{guild_id} Guilds getGuildByID
// Get guild by ID
// responses:
//   200: getGuildByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get guild by ID response body
// swagger:response getGuildByIDResponse
type getGuildByIDResponse struct {
	// in:body
	Body viewmodels.GetGuildByIDResponse
}

// swagger:route POST /guilds Guilds createGuild
// Create a guild
// responses:
//   201: createGuildResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create guild request
// swagger:parameters createGuild
type createGuildRequest struct {
	// in:body
	Body models.Guild
}

// Create guild response
// swagger:response createGuildResponse
type createGuildResponse struct {
	// in:body
	Body viewmodels.CreateGuildResponse
}

// swagger:route PUT /guilds/{guild_id} Guilds updateGuild
// Update a guild
// responses:
//   200: updateGuildResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create guild request
// swagger:parameters updateGuild
type updateGuildRequest struct {
	// in:body
	Body viewmodels.UpdateGuildRequest
}

// Update guild response
// swagger:response updateGuildResponse
type updateGuildResponse struct {
	// in:body
	Body viewmodels.UpdateGuildResponse
}

// swagger:route DELETE /guilds/{guild_id} Guilds deleteGuild
// Deletes a guild
// responses:
//   200: deleteGuildResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Delete guild response
// swagger:response deleteGuildResponse
type deleteGuildResponse struct {
	// in: body
	Body viewmodels.DeleteGuildResponse
}

// swagger:parameters getGuildByID
// swagger:parameters updateGuild
// swagger:parameters deleteGuild
type guildID struct {
	// Guild ID
	//
	// in: path
	// required: true
	GuildID string `json:"guild_id"`
}
