package docs

import "gitlab.com/BIC_Dev/guild-config-service/viewmodels"

// swagger:route POST /nitrado-setups NitradoSetups createNitradoSetup
// Create a Nitrado setup
// responses:
//   201: createNitradoSetupResponse
//   400: errorResponse
//   401: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create Nitrado setup request
// swagger:parameters createNitradoSetup
type createNitradoSetupRequest struct {
	// in:body
	Body viewmodels.CreateNitradoSetupRequest
}

// Create Nitrado setup response
// swagger:response createNitradoSetupResponse
type createNitradoSetupResponse struct {
	// in:body
	Body viewmodels.CreateNitradoSetupResponse
}
