package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /nitrado-tokens NitradoTokens getAllNitradoTokens
// Get all nitrado tokens
// responses:
//   200: getAllNitradoTokensResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get all nitrado tokens response body
// swagger:response getAllNitradoTokensResponse
type getAllNitradoTokensResponse struct {
	// in:body
	Body viewmodels.GetAllNitradoTokensResponse
}

// swagger:route GET /nitrado-tokens/{nitrado_token_id} NitradoTokens getNitradoTokenByID
// Get nitrado token by ID
// responses:
//   200: getNitradoTokenByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get nitrado token by ID response body
// swagger:response getNitradoTokenByIDResponse
type getNitradoTokenByIDResponse struct {
	// in:body
	Body viewmodels.GetNitradoTokenByIDResponse
}

// swagger:route POST /nitrado-tokens NitradoTokens createNitradoToken
// Create a nitrado token
// responses:
//   201: createNitradoTokenResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create nitrado token request
// swagger:parameters createNitradoToken
type createNitradoTokenRequest struct {
	// in:body
	Body models.NitradoToken
}

// Create nitrado token response
// swagger:response createNitradoTokenResponse
type createNitradoTokenResponse struct {
	// in:body
	Body viewmodels.CreateNitradoTokenResponse
}

// swagger:route PUT /nitrado-tokens/{nitrado_token_id} NitradoTokens updateNitradoToken
// Update a nitrado token
// responses:
//   200: updateNitradoTokenResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create nitrado token request
// swagger:parameters updateNitradoToken
type updateNitradoTokenRequest struct {
	// in:body
	Body viewmodels.UpdateNitradoTokenRequest
}

// Update nitrado token response
// swagger:response updateNitradoTokenResponse
type updateNitradoTokenResponse struct {
	// in:body
	Body viewmodels.UpdateNitradoTokenResponse
}

// swagger:route DELETE /nitrado-tokens/{nitrado_token_id} NitradoTokens deleteNitradoToken
// Deletes a nitrado token
// responses:
//   200: deleteNitradoTokenResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Delete nitrado token response
// swagger:response deleteNitradoTokenResponse
type deleteNitradoTokenResponse struct {
	// in: body
	Body viewmodels.DeleteNitradoTokenResponse
}

// swagger:parameters getNitradoTokenByID
// swagger:parameters updateNitradoToken
// swagger:parameters deleteNitradoToken
type nitradoTokenID struct {
	// Nitrado Token ID
	//
	// in: path
	// required: true
	NitradoTokenID int `json:"nitrado_token_id"`
}
