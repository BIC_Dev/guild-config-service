package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /output-channel-types OutputChannelTypes getAllOutputChannelTypes
// Get all output channel types
// responses:
//   200: getAllOutputChannelTypesResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get all output channel types response body
// swagger:response getAllOutputChannelTypesResponse
type getAllOutputChannelTypesResponse struct {
	// in:body
	Body viewmodels.GetAllOutputChannelTypesResponse
}

// swagger:route GET /output-channel-types/{output_channel_type_id} OutputChannelTypes getOutputChannelTypeByID
// Get output channel type by ID
// responses:
//   200: getOutputChannelTypeByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get output channel type by ID response body
// swagger:response getOutputChannelTypeByIDResponse
type getOutputChannelTypeByIDResponse struct {
	// in:body
	Body viewmodels.GetOutputChannelTypeByIDResponse
}

// swagger:route POST /output-channel-types OutputChannelTypes createOutputChannelType
// Create a output channel type
// responses:
//   201: createOutputChannelTypeResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create output channel type request
// swagger:parameters createOutputChannelType
type createOutputChannelTypeRequest struct {
	// in:body
	Body models.OutputChannelType
}

// Create output channel type response
// swagger:response createOutputChannelTypeResponse
type createOutputChannelTypeResponse struct {
	// in:body
	Body viewmodels.CreateOutputChannelTypeResponse
}

// swagger:route PUT /output-channel-types/{output_channel_type_id} OutputChannelTypes updateOutputChannelType
// Update a output channel type
// responses:
//   200: updateOutputChannelTypeResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create output channel type request
// swagger:parameters updateOutputChannelType
type updateOutputChannelTypeRequest struct {
	// in:body
	Body viewmodels.UpdateOutputChannelTypeRequest
}

// Update output channel type response
// swagger:response updateOutputChannelTypeResponse
type updateOutputChannelTypeResponse struct {
	// in:body
	Body viewmodels.UpdateOutputChannelTypeResponse
}

// swagger:route DELETE /output-channel-types/{output_channel_type_id} OutputChannelTypes deleteOutputChannelType
// Deletes a output channel type
// responses:
//   200: deleteOutputChannelTypeResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Delete output channel type response
// swagger:response deleteOutputChannelTypeResponse
type deleteOutputChannelTypeResponse struct {
	// in: body
	Body viewmodels.DeleteOutputChannelTypeResponse
}

// swagger:parameters getOutputChannelTypeByID
// swagger:parameters updateOutputChannelType
// swagger:parameters deleteOutputChannelType
type outputChannelTypeID struct {
	// OutputChannelType ID
	//
	// in: path
	// required: true
	OutputChannelTypeID string `json:"output_channel_type_id"`
}
