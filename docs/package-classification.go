// Package classification Guild Config Service
//
// Documentation of the Guild Config Service
//
//	 Schemes: http
//	 BasePath: /guild-config-service
//	 Version: 1.0.0
//	 Host: localhost:8082
//
//	 Consumes:
//	 - application/json
//
//	 Produces:
//	 - application/json
//
//	 Security:
//	 - ApiKey
//
//	SecurityDefinitions:
//	    ApiKey:
//	      type: apiKey
//	      in: header
//	      name: Service-Token
//
// swagger:meta
package docs

import "gitlab.com/BIC_Dev/guild-config-service/viewmodels"

// Error response body
// swagger:response errorResponse
type errorResponse struct {
	// in:body
	Body viewmodels.ErrorResponse
}

// Guild header
// swagger:parameters getBoostSettingByID
// swagger:parameters createBoostSetting
// swagger:parameters updateBoostSetting
// swagger:parameters deleteBoostSetting
// swagger:parameters getContactByID
// swagger:parameters createContact
// swagger:parameters updateContact
// swagger:parameters deleteContact
// swagger:parameters getGuildFeedByID
// swagger:parameters getGuildServiceContactByID
// swagger:parameters createGuildServiceContact
// swagger:parameters updateGuildServiceContact
// swagger:parameters deleteGuildServiceContact
// swagger:parameters createGuildServicePermission
// swagger:parameters updateGuildServicePermission
// swagger:parameters deleteGuildServicePermission
// swagger:parameters createGuildService
// swagger:parameters getGuildServiceByID
// swagger:parameters updateGuildService
// swagger:parameters deleteGuildService
// swagger:parameters getGuildByID
// swagger:parameters updateGuild
// swagger:parameters deleteGuild
// swagger:parameters getNitradoTokenByID
// swagger:parameters createNitradoToken
// swagger:parameters updateNitradoToken
// swagger:parameters deleteNitradoToken
// swagger:parameters getOutputChannelTypeByID
// swagger:parameters updateOutputChannelType
// swagger:parameters deleteOutputChannelType
// swagger:parameters getServerOutputChannelByID
// swagger:parameters createServerOutputChannel
// swagger:parameters updateServerOutputChannel
// swagger:parameters deleteServerOutputChannel
// swagger:parameters getServerOutputChannelSettingByID
// swagger:parameters createServerOutputChannelSetting
// swagger:parameters updateServerOutputChannelSetting
// swagger:parameters deleteServerOutputChannelSetting
// swagger:parameters getGuildOutputChannelByID
// swagger:parameters createGuildOutputChannel
// swagger:parameters updateGuildOutputChannel
// swagger:parameters deleteGuildOutputChannel
// swagger:parameters getServerTypeByID
// swagger:parameters updateServerType
// swagger:parameters deleteServerType
// swagger:parameters getServerByID
// swagger:parameters createServer
// swagger:parameters updateServer
// swagger:parameters deleteServer
// swagger:parameters getSetupByID
// swagger:parameters createSetup
// swagger:parameters updateSetup
// swagger:parameters deleteSetup
// swagger:parameters getStatus
// swagger:parameters createNitradoSetup
// swagger:parameters getArkTribeByID
// swagger:parameters createArkTribe
// swagger:parameters updateArkTribe
// swagger:parameters deleteArkTribe
// swagger:parameters getArkTribeMemberByID
// swagger:parameters createArkTribeMember
// swagger:parameters updateArkTribeMember
// swagger:parameters deleteArkTribeMember
type guildHeader struct {
	// in: header
	// name: Guild
	// required: true
	Guild string
}
