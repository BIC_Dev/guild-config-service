package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /server-output-channel-settings ServerOutputChannelSettings getAllServerOutputChannelSettings
// Get all server output channel settings
// responses:
//   200: getAllServerOutputChannelSettingsResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get all server output channel settings response body
// swagger:response getAllServerOutputChannelSettingsResponse
type getAllServerOutputChannelSettingsResponse struct {
	// in:body
	Body viewmodels.GetAllServerOutputChannelSettingsResponse
}

// swagger:route GET /server-output-channel-settings/{server_output_channel_setting_id} ServerOutputChannelSettings getServerOutputChannelSettingByID
// Get server output channel by ID
// responses:
//   200: getServerOutputChannelSettingByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get server output channel setting by ID response body
// swagger:response getServerOutputChannelSettingByIDResponse
type getServerOutputChannelSettingByIDResponse struct {
	// in:body
	Body viewmodels.GetServerOutputChannelSettingByIDResponse
}

// swagger:route POST /server-output-channel-settings ServerOutputChannelSettings createServerOutputChannelSetting
// Create a server output channel setting
// responses:
//   201: createServerOutputChannelSettingResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create server output channel setting request
// swagger:parameters createServerOutputChannelSetting
type createServerOutputChannelSettingRequest struct {
	// in:body
	Body models.ServerOutputChannelSetting
}

// Create server output channel setting response
// swagger:response createServerOutputChannelSettingResponse
type createServerOutputChannelSettingResponse struct {
	// in:body
	Body viewmodels.CreateServerOutputChannelSettingResponse
}

// swagger:route PUT /server-output-channel-settings/{server_output_channel_setting_id} ServerOutputChannelSettings updateServerOutputChannelSetting
// Update a server output channel setting
// responses:
//   200: updateServerOutputChannelSettingResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create server output channel setting request
// swagger:parameters updateServerOutputChannelSetting
type updateServerOutputChannelSettingRequest struct {
	// in:body
	Body viewmodels.UpdateServerOutputChannelSettingRequest
}

// Update server output channel setting response
// swagger:response updateServerOutputChannelSettingResponse
type updateServerOutputChannelSettingResponse struct {
	// in:body
	Body viewmodels.UpdateServerOutputChannelSettingResponse
}

// swagger:route DELETE /server-output-channel-settings/{server_output_channel_setting_id} ServerOutputChannelSettings deleteServerOutputChannelSetting
// Deletes a server output channel setting
// responses:
//   200: deleteServerOutputChannelSettingResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Delete server output channel setting response
// swagger:response deleteServerOutputChannelSettingResponse
type deleteServerOutputChannelSettingResponse struct {
	// in: body
	Body viewmodels.DeleteServerOutputChannelSettingResponse
}

// swagger:parameters getServerOutputChannelSettingByID
// swagger:parameters updateServerOutputChannelSetting
// swagger:parameters deleteServerOutputChannelSetting
type serverOutputChannelSettingID struct {
	// ServerOutputChannelSetting ID
	//
	// in: path
	// required: true
	ServerOutputChannelSettingID int `json:"server_output_channel_setting_id"`
}
