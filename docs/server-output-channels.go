package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /server-output-channels ServerOutputChannels getAllServerOutputChannels
// Get all server output channels
// responses:
//   200: getAllServerOutputChannelsResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get all server output channels response body
// swagger:response getAllServerOutputChannelsResponse
type getAllServerOutputChannelsResponse struct {
	// in:body
	Body viewmodels.GetAllServerOutputChannelsResponse
}

// swagger:route GET /server-output-channels/{server_output_channel_id} ServerOutputChannels getServerOutputChannelByID
// Get server output channel by ID
// responses:
//   200: getServerOutputChannelByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get server output channel by ID response body
// swagger:response getServerOutputChannelByIDResponse
type getServerOutputChannelByIDResponse struct {
	// in:body
	Body viewmodels.GetServerOutputChannelByIDResponse
}

// swagger:route POST /server-output-channels ServerOutputChannels createServerOutputChannel
// Create a server output channel
// responses:
//   201: createServerOutputChannelResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create server output channel request
// swagger:parameters createServerOutputChannel
type createServerOutputChannelRequest struct {
	// in:body
	Body models.ServerOutputChannel
}

// Create server output channel response
// swagger:response createServerOutputChannelResponse
type createServerOutputChannelResponse struct {
	// in:body
	Body viewmodels.CreateServerOutputChannelResponse
}

// swagger:route PUT /server-output-channels/{server_output_channel_id} ServerOutputChannels updateServerOutputChannel
// Update a server output channel
// responses:
//   200: updateServerOutputChannelResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create server output channel request
// swagger:parameters updateServerOutputChannel
type updateServerOutputChannelRequest struct {
	// in:body
	Body viewmodels.UpdateServerOutputChannelRequest
}

// Update server output channel response
// swagger:response updateServerOutputChannelResponse
type updateServerOutputChannelResponse struct {
	// in:body
	Body viewmodels.UpdateServerOutputChannelResponse
}

// swagger:route DELETE /server-output-channels/{server_output_channel_id} ServerOutputChannels deleteServerOutputChannel
// Deletes a server output channel
// responses:
//   200: deleteServerOutputChannelResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Delete server output channel response
// swagger:response deleteServerOutputChannelResponse
type deleteServerOutputChannelResponse struct {
	// in: body
	Body viewmodels.DeleteServerOutputChannelResponse
}

// swagger:parameters getServerOutputChannelByID
// swagger:parameters updateServerOutputChannel
// swagger:parameters deleteServerOutputChannel
type serverOutputChannelID struct {
	// ServerOutputChannel ID
	//
	// in: path
	// required: true
	ServerOutputChannelID int `json:"server_output_channel_id"`
}
