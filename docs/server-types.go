package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /server-types ServerTypes getAllServerTypes
// Get all server types
// responses:
//   200: getAllServerTypesResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get all server types response body
// swagger:response getAllServerTypesResponse
type getAllServerTypesResponse struct {
	// in:body
	Body viewmodels.GetAllServerTypesResponse
}

// swagger:route GET /server-types/{server_type_id} ServerTypes getServerTypeByID
// Get server type by ID
// responses:
//   200: getServerTypeByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get server type by ID response body
// swagger:response getServerTypeByIDResponse
type getServerTypeByIDResponse struct {
	// in:body
	Body viewmodels.GetServerTypeByIDResponse
}

// swagger:route POST /server-types ServerTypes createServerType
// Create a server type
// responses:
//   201: createServerTypeResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create server type request
// swagger:parameters createServerType
type createServerTypeRequest struct {
	// in:body
	Body models.ServerType
}

// Create server type response
// swagger:response createServerTypeResponse
type createServerTypeResponse struct {
	// in:body
	Body viewmodels.CreateServerTypeResponse
}

// swagger:route PUT /server-types/{server_type_id} ServerTypes updateServerType
// Update a server type
// responses:
//   200: updateServerTypeResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create server type request
// swagger:parameters updateServerType
type updateServerTypeRequest struct {
	// in:body
	Body viewmodels.UpdateServerTypeRequest
}

// Update server type response
// swagger:response updateServerTypeResponse
type updateServerTypeResponse struct {
	// in:body
	Body viewmodels.UpdateServerTypeResponse
}

// swagger:route DELETE /server-types/{server_type_id} ServerTypes deleteServerType
// Deletes a server type
// responses:
//   200: deleteServerTypeResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Delete server type response
// swagger:response deleteServerTypeResponse
type deleteServerTypeResponse struct {
	// in: body
	Body viewmodels.DeleteServerTypeResponse
}

// swagger:parameters getServerTypeByID
// swagger:parameters updateServerType
// swagger:parameters deleteServerType
type serverTypeID struct {
	// ServerType ID
	//
	// in: path
	// required: true
	ServerTypeID string `json:"server_type_id"`
}
