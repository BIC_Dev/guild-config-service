package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /servers Servers getAllServers
// Get all servers
// responses:
//   200: getAllServersResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get all servers response body
// swagger:response getAllServersResponse
type getAllServersResponse struct {
	// in:body
	Body viewmodels.GetAllServersResponse
}

// swagger:route GET /servers/{server_id} Servers getServerByID
// Get server by ID
// responses:
//   200: getServerByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get server by ID response body
// swagger:response getServerByIDResponse
type getServerByIDResponse struct {
	// in:body
	Body viewmodels.GetServerByIDResponse
}

// swagger:route POST /servers Servers createServer
// Create a server
// responses:
//   201: createServerResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create server request
// swagger:parameters createServer
type createServerRequest struct {
	// in:body
	Body models.Server
}

// Create server response
// swagger:response createServerResponse
type createServerResponse struct {
	// in:body
	Body viewmodels.CreateServerResponse
}

// swagger:route PUT /servers/{server_id} Servers updateServer
// Update a server
// responses:
//   200: updateServerResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create server request
// swagger:parameters updateServer
type updateServerRequest struct {
	// in:body
	Body viewmodels.UpdateServerRequest
}

// Update server response
// swagger:response updateServerResponse
type updateServerResponse struct {
	// in:body
	Body viewmodels.UpdateServerResponse
}

// swagger:route DELETE /servers/{server_id} Servers deleteServer
// Deletes a server
// responses:
//   200: deleteServerResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Delete server response
// swagger:response deleteServerResponse
type deleteServerResponse struct {
	// in: body
	Body viewmodels.DeleteServerResponse
}

// swagger:parameters getServerByID
// swagger:parameters updateServer
// swagger:parameters deleteServer
type serverPathParams struct {
	// Server ID
	//
	// in: path
	// required: true
	ServerID int `json:"server_id"`
}
