package docs

import (
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/viewmodels"
)

// swagger:route GET /setups Setups getAllSetups
// Get all setups
// responses:
//   200: getAllSetupsResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get all setups response body
// swagger:response getAllSetupsResponse
type getAllSetupsResponse struct {
	// in:body
	Body viewmodels.GetAllSetupsResponse
}

// swagger:route GET /setups/{setup_id} Setups getSetupByID
// Get setup by ID
// responses:
//   200: getSetupByIDResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Get setup by ID response body
// swagger:response getSetupByIDResponse
type getSetupByIDResponse struct {
	// in:body
	Body viewmodels.GetSetupByIDResponse
}

// swagger:route POST /setups Setups createSetup
// Create a setup
// responses:
//   201: createSetupResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create setup request
// swagger:parameters createSetup
type createSetupRequest struct {
	// in:body
	Body models.Setup
}

// Create setup response
// swagger:response createSetupResponse
type createSetupResponse struct {
	// in:body
	Body viewmodels.CreateSetupResponse
}

// swagger:route PUT /setups/{setup_id} Setups updateSetup
// Update a setup
// responses:
//   200: updateSetupResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create setup request
// swagger:parameters updateSetup
type updateSetupRequest struct {
	// in:body
	Body viewmodels.UpdateSetupRequest
}

// Update setup response
// swagger:response updateSetupResponse
type updateSetupResponse struct {
	// in:body
	Body viewmodels.UpdateSetupResponse
}

// swagger:route DELETE /setups/{setup_id} Setups deleteSetup
// Deletes a setup
// responses:
//   200: deleteSetupResponse
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Delete setup response
// swagger:response deleteSetupResponse
type deleteSetupResponse struct {
	// in: body
	Body viewmodels.DeleteSetupResponse
}

// swagger:parameters getSetupByID
// swagger:parameters updateSetup
// swagger:parameters deleteSetup
type setupID struct {
	// Setup ID
	//
	// in: path
	// required: true
	SetupID int `json:"setup_id"`
}
