package docs

import "gitlab.com/BIC_Dev/guild-config-service/viewmodels"

// swagger:route GET /status Status getStatus
// Get status of the service
// responses:
//   200: getStatusResponse

// Status response body
// swagger:response getStatusResponse
type getStatusResponseWrapper struct {
	// in:body
	Body viewmodels.GetStatusResponse
}
