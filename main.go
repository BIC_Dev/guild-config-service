package main

import (
	"context"
	"log"

	"gitlab.com/BIC_Dev/guild-config-service/configs"
	"gitlab.com/BIC_Dev/guild-config-service/controllers"
	"gitlab.com/BIC_Dev/guild-config-service/models"
	"gitlab.com/BIC_Dev/guild-config-service/routes"
	"gitlab.com/BIC_Dev/guild-config-service/utils/cache"
	"gitlab.com/BIC_Dev/guild-config-service/utils/db"
	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"

	"github.com/caarlos0/env"
	_ "github.com/pdrum/swagger-automation/docs"
)

// Config struct
type Config struct {
	Environment         string `env:"ENVIRONMENT,required"`
	ListenerPort        string `env:"LISTENER_PORT,required"`
	ServiceToken        string `env:"SERVICE_TOKEN,required"`
	BasePath            string `env:"BASE_PATH,required"`
	NitradoServiceToken string `env:"NITRADO_SERVICE_TOKEN,required"`
	Migrate             bool   `env:"MIGRATE"`
}

func main() {
	ctx := context.Background()
	cfg := Config{}
	if err := env.Parse(&cfg); err != nil {
		log.Fatal("FAILED TO LOAD CONFIG")
	}

	ctx = logging.AddValues(ctx,
		zap.String("scope", logging.GetFuncName()),
		zap.String("env", cfg.Environment),
		zap.String("listener_port", cfg.ListenerPort),
		zap.String("base_path", cfg.BasePath),
	)

	config := configs.GetConfig(ctx, cfg.Environment)
	cache := InitCache(ctx, config)
	gdb := InitDB(ctx, config)

	if cfg.Migrate {
		ctx = logging.AddValues(ctx, zap.Bool("migration", true))
		mgErr := db.Migrate(gdb,
			models.ServerType{},
			models.Guild{},
			models.NitradoToken{},
			models.ServerType{},
			models.Server{},
			models.BoostSetting{},
			models.Contact{},
			models.GuildService{},
			models.GuildServiceContact{},
			models.GuildServicePermission{},
			models.NitradoToken{},
			models.OutputChannelType{},
			models.ServerOutputChannel{},
			models.ServerOutputChannelSetting{},
			models.GuildOutputChannel{},
			models.Setup{},
			models.ArkTribe{},
			models.ArkTribeMember{},
		)
		if mgErr != nil {
			ctx = logging.AddValues(ctx, zap.NamedError("error", mgErr.Err))
		}
	}

	router := routes.GetRouter(ctx)
	controller := controllers.Controller{
		Config:              config,
		Cache:               cache,
		DB:                  gdb,
		NitradoServiceToken: cfg.NitradoServiceToken,
	}

	r := routes.Router{
		ServiceToken: cfg.ServiceToken,
		Port:         cfg.ListenerPort,
		BasePath:     cfg.BasePath,
		Controller:   &controller,
	}
	routes.AddRoutes(ctx, router, r)
}

// InitCache initializes the Redis cache
func InitCache(ctx context.Context, config *configs.Config) *cache.Cache {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))
	pool, err := cache.GetClient(ctx, config.Redis.Host, config.Redis.Port, config.Redis.Pool)

	if err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	return &cache.Cache{
		Client: pool,
	}
}

// InitDB initializes the DB connection
func InitDB(ctx context.Context, config *configs.Config) *gorm.DB {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))
	gdb, err := db.GetDB(config)

	if err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	return gdb
}
