package models

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// ArkTribeMember model
type ArkTribeMember struct {
	ID         uint      `gorm:"primary_key;auto_increment" json:"id"`
	MemberID   string    `gorm:"varchar(50)" json:"member_id"`
	ArkTribeID *uint     `json:"tribe_id"`
	Enabled    bool      `json:"enabled"`
	CreatedAt  time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt  time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	ArkTribe   *ArkTribe `gorm:"constraint:OnDelete:CASCADE" json:"ark_tribe,omitempty"`
}

// TableName func
func (ArkTribeMember) TableName() string {
	return "ark_tribe_members"
}

// Create adds a record to DB
func (t *ArkTribeMember) Create(ctx context.Context, gdb *gorm.DB) (*ArkTribeMember, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Create(&t)
	if db.Error != nil {
		return nil, &ModelError{
			Message: "Unable to create ArkTribeMember entry in DB",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindAll finds all records in DB
func (t *ArkTribeMember) FindAll(ctx context.Context, gdb *gorm.DB) (*[]ArkTribeMember, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	tables := []ArkTribeMember{}
	db := gdb.WithContext(ctx).Find(&tables)
	if db.Error != nil {
		return &[]ArkTribeMember{}, &ModelError{
			Message: "Failed to find all ArkTribeMember",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &tables, nil
}

// FindByID finds a record by ID
func (t *ArkTribeMember) FindByID(ctx context.Context, gdb *gorm.DB, id uint) (*ArkTribeMember, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "ark_tribe_members.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &ArkTribeMember{}, &ModelError{
			Message: "ArkTribeMember not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &ArkTribeMember{}, &ModelError{
			Message: "Failed query to find ArkTribeMember by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindByID finds a record by ID
func (t *ArkTribeMember) FindByMemberID(ctx context.Context, gdb *gorm.DB, id string) (*[]ArkTribeMember, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	tables := []ArkTribeMember{}
	db := gdb.WithContext(ctx)
	db = db.Find(&tables, "ark_tribe_members.member_id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &[]ArkTribeMember{}, &ModelError{
			Message: "ArkTribeMember not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &[]ArkTribeMember{}, &ModelError{
			Message: "Failed query to find ArkTribeMember by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &tables, nil
}

// Update updates a record by ID
func (t *ArkTribeMember) Update(ctx context.Context, gdb *gorm.DB, id uint, params map[string]interface{}) (*ArkTribeMember, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	params["updated_at"] = time.Now()
	db := gdb.WithContext(ctx).Model(&ArkTribeMember{}).Where("id = ?", id).Take(&ArkTribeMember{}).UpdateColumns(params)
	if db.Error != nil {
		return &ArkTribeMember{}, &ModelError{
			Message: "Failed to update ArkTribeMember",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t.FindByID(ctx, gdb, id)
}

// Delete deletes a record by ID
func (t *ArkTribeMember) Delete(ctx context.Context, gdb *gorm.DB, id uint) (int64, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Delete(&ArkTribeMember{}, id)
	if db.Error != nil {
		return 0, &ModelError{
			Message: "Failed to delete ArkTribeMember",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return db.RowsAffected, nil
}
