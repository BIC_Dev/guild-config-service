package models

import (
	"context"
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

type ArkTribeJSONB []string

// ArkTribe model
type ArkTribe struct {
	ID              uint              `gorm:"primary_key;auto_increment" json:"id"`
	Name            string            `gorm:"varchar(50)" json:"name"`
	TribeIDs        ArkTribeJSONB     `gorm:"type:jsonb;default:'[]';not null" json:"tribe_ids"`
	LeaderID        string            `gorm:"varchar(20)" json:"leader_id"`
	GuildID         *string           `gorm:"varchar(20)" json:"guild_id"`
	Enabled         bool              `json:"enabled"`
	CreatedAt       time.Time         `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt       time.Time         `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	Guild           *Guild            `gorm:"constraint:OnDelete:CASCADE" json:"guild,omitempty"`
	ArkTribeMembers []*ArkTribeMember `json:"ark_tribe_members,omitempty"`
}

// TableName func
func (ArkTribe) TableName() string {
	return "ark_tribes"
}

// Create adds a record to DB
func (t *ArkTribe) Create(ctx context.Context, gdb *gorm.DB) (*ArkTribe, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Create(&t)
	if db.Error != nil {
		return nil, &ModelError{
			Message: "Unable to create ArkTribe entry in DB",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindAll finds all records in DB
func (t *ArkTribe) FindAll(ctx context.Context, gdb *gorm.DB) (*[]ArkTribe, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	tables := []ArkTribe{}
	db := gdb.WithContext(ctx).Find(&tables)
	if db.Error != nil {
		return &[]ArkTribe{}, &ModelError{
			Message: "Failed to find all ArkTribe",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &tables, nil
}

// FindByID finds a record by ID
func (t *ArkTribe) FindByID(ctx context.Context, gdb *gorm.DB, id uint) (*ArkTribe, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "ark_tribes.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &ArkTribe{}, &ModelError{
			Message: "ArkTribe not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &ArkTribe{}, &ModelError{
			Message: "Failed query to find ArkTribe by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// Update updates a record by ID
func (t *ArkTribe) Update(ctx context.Context, gdb *gorm.DB, id uint, params map[string]interface{}) (*ArkTribe, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	params["updated_at"] = time.Now()
	if val, ok := params["tribe_ids"]; ok {
		mVal, mErr := json.Marshal(val)
		if mErr != nil {
			return nil, &ModelError{
				Message: "Failed to marshal tribe IDs",
				Err:     mErr,
			}
		}

		params["tribe_ids"] = mVal
	}
	db := gdb.WithContext(ctx).Model(&ArkTribe{}).Where("id = ?", id).Take(&ArkTribe{}).UpdateColumns(params)
	if db.Error != nil {
		return &ArkTribe{}, &ModelError{
			Message: "Failed to update ArkTribe",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t.FindByID(ctx, gdb, id)
}

// Delete deletes a record by ID
func (t *ArkTribe) Delete(ctx context.Context, gdb *gorm.DB, id uint) (int64, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Delete(&ArkTribe{}, id)
	if db.Error != nil {
		return 0, &ModelError{
			Message: "Failed to delete ArkTribe",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return db.RowsAffected, nil
}

func (j ArkTribeJSONB) Value() (driver.Value, error) {
	valueString, err := json.Marshal(j)
	return string(valueString), err
}

func (j *ArkTribeJSONB) Scan(value interface{}) error {
	var bytes []byte
	if val, ok := value.([]byte); ok {
		bytes = val
	} else if val, ok := value.(string); ok {
		bytes = []byte(val)
	} else {
		return errors.New(fmt.Sprint("Failed to convert JSONB value to []byte: ", value))
	}

	result := []string{}
	err := json.Unmarshal(bytes, &result)
	*j = result

	return err
}
