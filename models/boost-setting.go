package models

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// BoostSetting struct
type BoostSetting struct {
	ID             uint      `gorm:"primary_key;auto_increment" json:"id"`
	Code           string    `gorm:"varchar(10)" json:"code"`
	Message        string    `gorm:"text" json:"message"`
	WelcomeMessage string    `gorm:"text" json:"welcome_message"`
	BoostEnabled   bool      `json:"boost_enabled"`
	Enabled        bool      `json:"enabled"`
	CreatedAt      time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt      time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

// TableName func
func (BoostSetting) TableName() string {
	return "boost_settings"
}

// Create adds a record to DB
func (t *BoostSetting) Create(ctx context.Context, gdb *gorm.DB) (*BoostSetting, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Create(&t)
	if db.Error != nil {
		return nil, &ModelError{
			Message: "Unable to create BoostSetting entry in DB",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindAll finds all records in DB
func (t *BoostSetting) FindAll(ctx context.Context, gdb *gorm.DB) (*[]BoostSetting, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	ms := []BoostSetting{}
	db := gdb.WithContext(ctx).Find(&ms)
	if db.Error != nil {
		return &[]BoostSetting{}, &ModelError{
			Message: "Failed to find all BoostSettings",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &ms, nil
}

// FindByID finds a record by ID
func (t *BoostSetting) FindByID(ctx context.Context, gdb *gorm.DB, id uint) (*BoostSetting, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "boost_settings.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &BoostSetting{}, &ModelError{
			Message: "BoostSetting not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &BoostSetting{}, &ModelError{
			Message: "Failed query to find BoostSetting by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// Update updates a record by ID
func (t *BoostSetting) Update(ctx context.Context, gdb *gorm.DB, id uint, params map[string]interface{}) (*BoostSetting, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	params["updated_at"] = time.Now()
	db := gdb.WithContext(ctx).Model(&BoostSetting{}).Where("id = ?", id).Take(&BoostSetting{}).UpdateColumns(params)
	if db.Error != nil {
		return &BoostSetting{}, &ModelError{
			Message: "Failed to update BoostSetting",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t.FindByID(ctx, gdb, id)
}

// Delete deletes a record by ID
func (t *BoostSetting) Delete(ctx context.Context, gdb *gorm.DB, id uint) (int64, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Delete(&BoostSetting{}, id)
	if db.Error != nil {
		return 0, &ModelError{
			Message: "Failed to delete BoostSetting",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return db.RowsAffected, nil
}
