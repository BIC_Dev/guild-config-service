package models

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// Contact struct
type Contact struct {
	ID        string    `gorm:"primary_key;varchar(20)" json:"id"`
	Name      string    `gorm:"varchar(50)" json:"name"`
	Enabled   bool      `json:"enabled"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

// TableName func
func (Contact) TableName() string {
	return "contacts"
}

// Create adds a record to DB
func (t *Contact) Create(ctx context.Context, gdb *gorm.DB) (*Contact, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Create(&t)
	if db.Error != nil {
		return nil, &ModelError{
			Message: "Unable to create Contact entry in DB",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindAll finds all records in DB
func (t *Contact) FindAll(ctx context.Context, gdb *gorm.DB) (*[]Contact, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	ms := []Contact{}
	db := gdb.WithContext(ctx).Find(&ms)
	if db.Error != nil {
		return &[]Contact{}, &ModelError{
			Message: "Failed to find all Contact",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &ms, nil
}

// FindByID finds a record by ID
func (t *Contact) FindByID(ctx context.Context, gdb *gorm.DB, id string) (*Contact, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "contacts.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &Contact{}, &ModelError{
			Message: "Contact not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &Contact{}, &ModelError{
			Message: "Failed query to find Contact by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// Update updates a record by ID
func (t *Contact) Update(ctx context.Context, gdb *gorm.DB, id string, params map[string]interface{}) (*Contact, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	params["updated_at"] = time.Now()
	db := gdb.WithContext(ctx).Model(&Contact{}).Where("id = ?", id).Take(&Contact{}).UpdateColumns(params)
	if db.Error != nil {
		return &Contact{}, &ModelError{
			Message: "Failed to update Contact",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t.FindByID(ctx, gdb, id)
}

// Delete deletes a record by ID
func (t *Contact) Delete(ctx context.Context, gdb *gorm.DB, id string) (int64, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Delete(&Contact{}, id)
	if db.Error != nil {
		return 0, &ModelError{
			Message: "Failed to delete Contact",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return db.RowsAffected, nil
}
