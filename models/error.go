package models

// ModelError struct
type ModelError struct {
	Message string
	Err     error
}
