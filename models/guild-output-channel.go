package models

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// GuildOutputChannel struct
type GuildOutputChannel struct {
	ID                  uint               `gorm:"primary_key;auto_increment" json:"id"`
	ChannelID           string             `gorm:"varchar(20);index" json:"channel_id"`
	OutputChannelTypeID *string            `gorm:"varchar(10)" json:"output_channel_type_id"`
	GuildID             *string            `gorm:"varchar(20)" json:"guild_id"`
	Enabled             bool               `json:"enabled"`
	CreatedAt           time.Time          `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt           time.Time          `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	OutputChannelType   *OutputChannelType `gorm:"constraint:OnDelete:CASCADE" json:"output_channel_type,omitempty"`
	Guild               *Guild             `gorm:"constraint:OnDelete:CASCADE" json:"guild,omitempty"`
}

// TableName func
func (GuildOutputChannel) TableName() string {
	return "guild_output_channels"
}

// Create adds a record to DB
func (t *GuildOutputChannel) Create(ctx context.Context, gdb *gorm.DB) (*GuildOutputChannel, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Create(&t)
	if db.Error != nil {
		return nil, &ModelError{
			Message: "Unable to create GuildOutputChannel entry in DB",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindAll finds all records in DB
func (t *GuildOutputChannel) FindAll(ctx context.Context, gdb *gorm.DB) (*[]GuildOutputChannel, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	tables := []GuildOutputChannel{}
	db := gdb.WithContext(ctx).Find(&tables)
	if db.Error != nil {
		return &[]GuildOutputChannel{}, &ModelError{
			Message: "Failed to find all GuildOutputChannels",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &tables, nil
}

// FindByID finds a record by ID
func (t *GuildOutputChannel) FindByID(ctx context.Context, gdb *gorm.DB, id uint) (*GuildOutputChannel, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "guild_output_channels.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &GuildOutputChannel{}, &ModelError{
			Message: "OutputChannel not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &GuildOutputChannel{}, &ModelError{
			Message: "Failed query to find GuildOutputChannel by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// Update updates a record by ID
func (t *GuildOutputChannel) Update(ctx context.Context, gdb *gorm.DB, id uint, params map[string]interface{}) (*GuildOutputChannel, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	params["updated_at"] = time.Now()
	db := gdb.WithContext(ctx).Model(&GuildOutputChannel{}).Where("id = ?", id).Take(&GuildOutputChannel{}).UpdateColumns(params)
	if db.Error != nil {
		return &GuildOutputChannel{}, &ModelError{
			Message: "Failed to update OutputChannel",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t.FindByID(ctx, gdb, id)
}

// Delete deletes a record by ID
func (t *GuildOutputChannel) Delete(ctx context.Context, gdb *gorm.DB, id uint) (int64, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Delete(&GuildOutputChannel{}, id)
	if db.Error != nil {
		return 0, &ModelError{
			Message: "Failed to delete GuildOutputChannel",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return db.RowsAffected, nil
}
