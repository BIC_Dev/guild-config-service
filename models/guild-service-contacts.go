package models

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// GuildServiceContact struct
type GuildServiceContact struct {
	ID             uint          `gorm:"primary_key;auto_increment" json:"id"`
	GuildServiceID *uint         `json:"guild_service_id"`
	ContactID      *string       `json:"contact_id"`
	CreatedAt      time.Time     `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt      time.Time     `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	GuildService   *GuildService `gorm:"constraint:OnDelete:CASCADE" json:"guild_service,omitempty"`
	Contact        *Contact      `gorm:"constraint:OnDelete:CASCADE" json:"contact,omitempty"`
}

// TableName func
func (GuildServiceContact) TableName() string {
	return "guild_service_contacts"
}

// Create adds a record to DB
func (t *GuildServiceContact) Create(ctx context.Context, gdb *gorm.DB) (*GuildServiceContact, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Create(&t)
	if db.Error != nil {
		return nil, &ModelError{
			Message: "Unable to create GuildServiceContact entry in DB",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindAll finds all records in DB
func (t *GuildServiceContact) FindAll(ctx context.Context, gdb *gorm.DB) (*[]GuildServiceContact, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	ms := []GuildServiceContact{}
	db := gdb.WithContext(ctx).Find(&ms)
	if db.Error != nil {
		return &[]GuildServiceContact{}, &ModelError{
			Message: "Failed to find all GuildServiceContact",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &ms, nil
}

// FindByGuildService finds all records in DB
func (t *GuildServiceContact) FindByGuildService(ctx context.Context, gdb *gorm.DB, guildServiceID uint) (*[]GuildServiceContact, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	ms := []GuildServiceContact{}
	db := gdb.WithContext(ctx).Find(&ms, "guild_service_contacts.guild_service_id = ?", guildServiceID)
	if db.Error != nil {
		return &[]GuildServiceContact{}, &ModelError{
			Message: "Failed to find all GuildServiceContact by Guild Service",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &ms, nil
}

// FindByID finds a record by ID
func (t *GuildServiceContact) FindByID(ctx context.Context, gdb *gorm.DB, id uint) (*GuildServiceContact, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "guild_service_contacts.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &GuildServiceContact{}, &ModelError{
			Message: "GuildServiceContact not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &GuildServiceContact{}, &ModelError{
			Message: "Failed query to find GuildServiceContact by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindByGuildServiceAndContact finds a record by ID
func (t *GuildServiceContact) FindByGuildServiceAndContact(ctx context.Context, gdb *gorm.DB, guildServiceID uint, contactID string) (*GuildServiceContact, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "guild_service_contacts.guild_service_id = ? AND guild_service_contacts.contact_id = ?", guildServiceID, contactID)
	if db.Error == gorm.ErrRecordNotFound {
		return &GuildServiceContact{}, &ModelError{
			Message: "GuildServiceContact not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &GuildServiceContact{}, &ModelError{
			Message: "Failed query to find GuildServiceContact by Guild Service and Contact",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// Update updates a record by ID
func (t *GuildServiceContact) Update(ctx context.Context, gdb *gorm.DB, id uint, params map[string]interface{}) (*GuildServiceContact, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	params["updated_at"] = time.Now()
	db := gdb.WithContext(ctx).Model(&GuildServiceContact{}).Where("id = ?", id).Take(&GuildServiceContact{}).UpdateColumns(params)
	if db.Error != nil {
		return &GuildServiceContact{}, &ModelError{
			Message: "Failed to update GuildServiceContact",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t.FindByID(ctx, gdb, id)
}

// Delete deletes a record by ID
func (t *GuildServiceContact) Delete(ctx context.Context, gdb *gorm.DB, id uint) (int64, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Delete(&GuildServiceContact{}, id)
	if db.Error != nil {
		return 0, &ModelError{
			Message: "Failed to delete GuildServiceContact",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return db.RowsAffected, nil
}
