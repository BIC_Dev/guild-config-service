package models

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// GuildServicePermission struct
type GuildServicePermission struct {
	ID             uint          `gorm:"primary_key;auto_increment" json:"id"`
	CommandName    string        `gorm:"varchar(20)" json:"command_name"`
	RoleID         string        `gorm:"varchar(20)" json:"role_id"`
	GuildServiceID *uint         `gorm:"index" json:"guild_service_id"`
	Enabled        bool          `json:"enabled"`
	CreatedAt      time.Time     `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt      time.Time     `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	GuildService   *GuildService `gorm:"constraint:OnDelete:CASCADE" json:"guild_service,omitempty"`
}

// TableName func
func (GuildServicePermission) TableName() string {
	return "guild_service_permissions"
}

// Create adds a record to DB
func (t *GuildServicePermission) Create(ctx context.Context, gdb *gorm.DB) (*GuildServicePermission, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Create(&t)
	if db.Error != nil {
		return nil, &ModelError{
			Message: "Unable to create GuildServicePermission entry in DB",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindAll finds all records in DB
func (t *GuildServicePermission) FindAll(ctx context.Context, gdb *gorm.DB) (*[]GuildServicePermission, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	tables := []GuildServicePermission{}
	db := gdb.WithContext(ctx).Find(&tables)
	if db.Error != nil {
		return &[]GuildServicePermission{}, &ModelError{
			Message: "Failed to find all GuildServicePermission",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &tables, nil
}

// FindByGuild finds all records in DB
func (t *GuildServicePermission) FindByGuild(ctx context.Context, gdb *gorm.DB, guildID string) (*[]GuildServicePermission, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	tables := []GuildServicePermission{}
	db := gdb.WithContext(ctx).Find(&tables, "guild_service_permissions.guild_id = ?", guildID)
	if db.Error != nil {
		return &[]GuildServicePermission{}, &ModelError{
			Message: "Failed to find all GuildServicePermission by guild",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &tables, nil
}

// FindByID finds a record by ID
func (t *GuildServicePermission) FindByID(ctx context.Context, gdb *gorm.DB, id uint) (*GuildServicePermission, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "guild_service_permissions.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &GuildServicePermission{}, &ModelError{
			Message: "GuildServicePermission not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &GuildServicePermission{}, &ModelError{
			Message: "Failed query to find GuildServicePermission by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// Update updates a record by ID
func (t *GuildServicePermission) Update(ctx context.Context, gdb *gorm.DB, id uint, params map[string]interface{}) (*GuildServicePermission, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	params["updated_at"] = time.Now()
	db := gdb.WithContext(ctx).Model(&GuildServicePermission{}).Where("id = ?", id).Take(&GuildServicePermission{}).UpdateColumns(params)
	if db.Error != nil {
		return &GuildServicePermission{}, &ModelError{
			Message: "Failed to update GuildServicePermission",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t.FindByID(ctx, gdb, id)
}

// Delete deletes a record by ID
func (t *GuildServicePermission) Delete(ctx context.Context, gdb *gorm.DB, id uint) (int64, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Delete(&GuildServicePermission{}, id)
	if db.Error != nil {
		return 0, &ModelError{
			Message: "Failed to delete GuildServicePermission",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return db.RowsAffected, nil
}
