package models

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// Determines which bot services the guild has access to

// GuildService struct
type GuildService struct {
	ID                      uint                      `gorm:"primary_key;auto_increment" json:"id"`
	Name                    string                    `gorm:"varchar(50)" json:"name"`
	GuildID                 *string                   `gorm:"varchar(20);index" json:"guild_id"`
	Enabled                 bool                      `json:"enabled"`
	CreatedAt               time.Time                 `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt               time.Time                 `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	Guild                   *Guild                    `gorm:"constraint:OnDelete:CASCADE" json:"guild,omitempty"`
	GuildServicePermissions []*GuildServicePermission `json:"guild_service_permissions,omitempty"`
}

// TableName func
func (GuildService) TableName() string {
	return "guild_services"
}

// Create adds a record to DB
func (t *GuildService) Create(ctx context.Context, gdb *gorm.DB) (*GuildService, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Create(&t)
	if db.Error != nil {
		return nil, &ModelError{
			Message: "Unable to create GuildService entry in DB",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindAll finds all records in DB
func (t *GuildService) FindAll(ctx context.Context, gdb *gorm.DB) (*[]GuildService, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	tables := []GuildService{}
	db := gdb.WithContext(ctx).Find(&tables)
	if db.Error != nil {
		return &[]GuildService{}, &ModelError{
			Message: "Failed to find all GuildService",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &tables, nil
}

// FindByGuild finds all records in DB
func (t *GuildService) FindByGuild(ctx context.Context, gdb *gorm.DB, guildID string) (*[]GuildService, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	tables := []GuildService{}
	db := gdb.WithContext(ctx).Find(&tables, "guild_services.guild_id = ?", guildID)
	if db.Error != nil {
		return &[]GuildService{}, &ModelError{
			Message: "Failed to find all GuildService by guild",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &tables, nil
}

// FindByID finds a record by ID
func (t *GuildService) FindByID(ctx context.Context, gdb *gorm.DB, id uint) (*GuildService, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "guild_services.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &GuildService{}, &ModelError{
			Message: "GuildService not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &GuildService{}, &ModelError{
			Message: "Failed query to find GuildService by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindByGuildAndName finds a record by ID
func (t *GuildService) FindByGuildAndName(ctx context.Context, gdb *gorm.DB, guildID string, name string) (*GuildService, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "guild_services.name = ? AND guild_services.guild_id = ?", name, guildID)
	if db.Error == gorm.ErrRecordNotFound {
		return &GuildService{}, &ModelError{
			Message: "GuildService not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &GuildService{}, &ModelError{
			Message: "Failed query to find GuildService by Name and GuildID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// Update updates a record by ID
func (t *GuildService) Update(ctx context.Context, gdb *gorm.DB, id uint, params map[string]interface{}) (*GuildService, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	params["updated_at"] = time.Now()
	db := gdb.WithContext(ctx).Model(&GuildService{}).Where("id = ?", id).Take(&GuildService{}).UpdateColumns(params)
	if db.Error != nil {
		return &GuildService{}, &ModelError{
			Message: "Failed to update GuildService",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t.FindByID(ctx, gdb, id)
}

// Delete deletes a record by ID
func (t *GuildService) Delete(ctx context.Context, gdb *gorm.DB, id uint) (int64, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Delete(&GuildService{}, id)
	if db.Error != nil {
		return 0, &ModelError{
			Message: "Failed to delete GuildService",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return db.RowsAffected, nil
}
