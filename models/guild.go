package models

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// Guild model
type Guild struct {
	ID                  string                `gorm:"primary_key;varchar(20)" json:"id"`
	Name                string                `gorm:"varchar(200)" json:"name"`
	Enabled             bool                  `json:"enabled"`
	CreatedAt           time.Time             `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt           time.Time             `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	Servers             []*Server             `json:"servers,omitempty"`
	NitradoTokens       []*NitradoToken       `json:"nitrado_tokens,omitempty"`
	GuildServices       []*GuildService       `json:"guild_services,omitempty"`
	Setups              []*Setup              `json:"setups,omitempty"`
	GuildOutputChannels []*GuildOutputChannel `json:"guild_output_channels,omitempty"`
	ArkTribes           []*ArkTribe           `json:"ark_tribes,omitempty"`
}

// TableName func
func (Guild) TableName() string {
	return "guilds"
}

// Create adds a record to DB
func (t *Guild) Create(ctx context.Context, gdb *gorm.DB) (*Guild, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Create(&t)
	if db.Error != nil {
		return nil, &ModelError{
			Message: "Unable to create Guild entry in DB",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindAll finds all records in DB
func (t *Guild) FindAll(ctx context.Context, gdb *gorm.DB) (*[]Guild, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	guilds := []Guild{}
	db := gdb.WithContext(ctx).Find(&guilds)
	if db.Error != nil {
		return &[]Guild{}, &ModelError{
			Message: "Failed to find all Guilds",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &guilds, nil
}

// FindByID finds a record by ID
func (t *Guild) FindByID(ctx context.Context, gdb *gorm.DB, id string) (*Guild, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "guilds.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &Guild{}, &ModelError{
			Message: "Guild not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &Guild{}, &ModelError{
			Message: "Failed query to find Guild by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindDetailedByID finds a record by ID
func (t *Guild) FindDetailedByID(ctx context.Context, gdb *gorm.DB, id string) (*Guild, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Model(Guild{})
	db = db.Preload("Servers").Preload("Servers.BoostSetting").Preload("Servers.NitradoToken").Preload("Servers.ServerOutputChannels").Preload("Servers.ServerOutputChannels.OutputChannelType").Preload("Servers.ServerOutputChannels.ServerOutputChannelSettings")
	db = db.Preload("NitradoTokens")
	db = db.Preload("GuildServices").Preload("GuildServices.GuildServicePermissions")
	db = db.Preload("Setups").Preload("Setups.Contact")
	db = db.Preload("GuildOutputChannels")
	db = db.Preload("ArkTribes").Preload("ArkTribes.ArkTribeMembers")
	db = db.Take(&t, "guilds.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &Guild{}, &ModelError{
			Message: "Guild detailed not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &Guild{}, &ModelError{
			Message: "Failed query to find Guild detailed by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// Update updates a record by ID
func (t *Guild) Update(ctx context.Context, gdb *gorm.DB, id string, params map[string]interface{}) (*Guild, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	params["updated_at"] = time.Now()
	db := gdb.WithContext(ctx).Model(&Guild{}).Where("id = ?", id).Take(&Guild{}).UpdateColumns(params)
	if db.Error != nil {
		return &Guild{}, &ModelError{
			Message: "Failed to update Guild",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t.FindByID(ctx, gdb, id)
}

// Delete deletes a record by ID
func (t *Guild) Delete(ctx context.Context, gdb *gorm.DB, id string) (int64, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Delete(&Guild{}, id)
	if db.Error != nil {
		return 0, &ModelError{
			Message: "Failed to delete Guild",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return db.RowsAffected, nil
}
