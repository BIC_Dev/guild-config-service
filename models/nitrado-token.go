package models

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// NitradoToken model
type NitradoToken struct {
	ID         uint      `gorm:"primary_key;auto_increment" json:"id"`
	Token      string    `gorm:"varchar(50)" json:"token"`
	TokenValue string    `gorm:"varchar(250)" json:"token_value"`
	GuildID    *string   `gorm:"varchar(20)" json:"guild_id"`
	Enabled    bool      `json:"enabled"`
	CreatedAt  time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt  time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	Guild      *Guild    `gorm:"constraint:OnDelete:SET NULL" json:"guild,omitempty"`
}

// TableName func
func (NitradoToken) TableName() string {
	return "nitrado_tokens"
}

// Create adds a record to DB
func (t *NitradoToken) Create(ctx context.Context, gdb *gorm.DB) (*NitradoToken, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Create(&t)
	if db.Error != nil {
		return nil, &ModelError{
			Message: "Unable to create NitradoToken entry in DB",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindAll finds all records in DB
func (t *NitradoToken) FindAll(ctx context.Context, gdb *gorm.DB) (*[]NitradoToken, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	nitradoTokens := []NitradoToken{}
	db := gdb.WithContext(ctx).Find(&nitradoTokens)
	if db.Error != nil {
		return &[]NitradoToken{}, &ModelError{
			Message: "Failed to find all NitradoTokens",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &nitradoTokens, nil
}

// FindByID finds a record by ID
func (t *NitradoToken) FindByID(ctx context.Context, gdb *gorm.DB, id uint) (*NitradoToken, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "nitrado_tokens.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &NitradoToken{}, &ModelError{
			Message: "NitradoToken not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &NitradoToken{}, &ModelError{
			Message: "Failed query to find NitradoToken by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindByGuild finds a record by ID
func (t *NitradoToken) FindByGuild(ctx context.Context, gdb *gorm.DB, guildID string) (*[]NitradoToken, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	nitradoTokens := []NitradoToken{}

	db := gdb.WithContext(ctx)
	db = db.Find(&nitradoTokens, "nitrado_tokens.guild_id = ?", guildID)
	if db.Error == gorm.ErrRecordNotFound {
		return &[]NitradoToken{}, &ModelError{
			Message: "NitradoToken not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &[]NitradoToken{}, &ModelError{
			Message: "Failed query to find NitradoTokens by Guild",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &nitradoTokens, nil
}

// Update updates a record by ID
func (t *NitradoToken) Update(ctx context.Context, gdb *gorm.DB, id uint, params map[string]interface{}) (*NitradoToken, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Model(&NitradoToken{}).Where("id = ?", id).Take(&NitradoToken{}).UpdateColumns(params)
	if db.Error != nil {
		return &NitradoToken{}, &ModelError{
			Message: "Failed to update NitradoToken",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t.FindByID(ctx, gdb, id)
}

// Delete deletes a record by ID
func (t *NitradoToken) Delete(ctx context.Context, gdb *gorm.DB, id uint) (int64, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Delete(&NitradoToken{}, id)
	if db.Error != nil {
		return 0, &ModelError{
			Message: "Failed to delete NitradoToken",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return db.RowsAffected, nil
}
