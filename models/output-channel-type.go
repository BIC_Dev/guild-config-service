package models

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// OutputChannelType struct
type OutputChannelType struct {
	ID          string    `gorm:"primary_key" json:"id"`
	Description string    `gorm:"text" json:"description"`
	Enabled     bool      `json:"enabled"`
	CreatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

// TableName func
func (OutputChannelType) TableName() string {
	return "output_channel_types"
}

// Create adds a record to DB
func (t *OutputChannelType) Create(ctx context.Context, gdb *gorm.DB) (*OutputChannelType, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Create(&t)
	if db.Error != nil {
		return nil, &ModelError{
			Message: "Unable to create OutputChannelType entry in DB",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindAll finds all records in DB
func (t *OutputChannelType) FindAll(ctx context.Context, gdb *gorm.DB) (*[]OutputChannelType, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	tables := []OutputChannelType{}
	db := gdb.WithContext(ctx).Find(&tables)
	if db.Error != nil {
		return &[]OutputChannelType{}, &ModelError{
			Message: "Failed to find all OutputChannelType",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &tables, nil
}

// FindByID finds a record by ID
func (t *OutputChannelType) FindByID(ctx context.Context, gdb *gorm.DB, id string) (*OutputChannelType, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "output_channel_types.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &OutputChannelType{}, &ModelError{
			Message: "OutputChannelType not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &OutputChannelType{}, &ModelError{
			Message: "Failed query to find OutputChannelType by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// Update updates a record by ID
func (t *OutputChannelType) Update(ctx context.Context, gdb *gorm.DB, id string, params map[string]interface{}) (*OutputChannelType, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	params["updated_at"] = time.Now()
	db := gdb.WithContext(ctx).Model(&OutputChannelType{}).Where("id = ?", id).Take(&OutputChannelType{}).UpdateColumns(params)
	if db.Error != nil {
		return &OutputChannelType{}, &ModelError{
			Message: "Failed to update OutputChannelType",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t.FindByID(ctx, gdb, id)
}

// Delete deletes a record by ID
func (t *OutputChannelType) Delete(ctx context.Context, gdb *gorm.DB, id string) (int64, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Delete(&OutputChannelType{}, id)
	if db.Error != nil {
		return 0, &ModelError{
			Message: "Failed to delete OutputChannelType",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return db.RowsAffected, nil
}
