package models

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// ServerOutputChannelSetting struct
type ServerOutputChannelSetting struct {
	ID                    uint                 `gorm:"primary_key;auto_increment" json:"id"`
	ServerOutputChannelID uint                 `json:"server_output_channel_id"`
	SettingName           string               `gorm:"varchar(250)" json:"setting_name"`
	SettingValue          string               `gorm:"varchar(250)" json:"setting_value"`
	Enabled               bool                 `json:"enabled"`
	CreatedAt             time.Time            `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt             time.Time            `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	ServerOutputChannel   *ServerOutputChannel `gorm:"constraint:OnDelete:CASCADE" json:"server_output_channel,omitempty"`
}

// TableName func
func (ServerOutputChannelSetting) TableName() string {
	return "server_output_channel_settings"
}

// Create adds a record to DB
func (t *ServerOutputChannelSetting) Create(ctx context.Context, gdb *gorm.DB) (*ServerOutputChannelSetting, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Create(&t)
	if db.Error != nil {
		return nil, &ModelError{
			Message: "Unable to create ServerOutputChannelSetting entry in DB",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindAll finds all records in DB
func (t *ServerOutputChannelSetting) FindAll(ctx context.Context, gdb *gorm.DB) (*[]ServerOutputChannelSetting, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	tables := []ServerOutputChannelSetting{}
	db := gdb.WithContext(ctx).Find(&tables)
	if db.Error != nil {
		return &[]ServerOutputChannelSetting{}, &ModelError{
			Message: "Failed to find all ServerOutputChannelSetting",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &tables, nil
}

// FindByID finds a record by ID
func (t *ServerOutputChannelSetting) FindByID(ctx context.Context, gdb *gorm.DB, id uint) (*ServerOutputChannelSetting, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "server_output_channel_settings.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &ServerOutputChannelSetting{}, &ModelError{
			Message: "ServerOutputChannelSetting not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &ServerOutputChannelSetting{}, &ModelError{
			Message: "Failed query to find ServerOutputChannelSetting by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// Update updates a record by ID
func (t *ServerOutputChannelSetting) Update(ctx context.Context, gdb *gorm.DB, id uint, params map[string]interface{}) (*ServerOutputChannelSetting, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	params["updated_at"] = time.Now()
	db := gdb.WithContext(ctx).Model(&ServerOutputChannelSetting{}).Where("id = ?", id).Take(&ServerOutputChannelSetting{}).UpdateColumns(params)
	if db.Error != nil {
		return &ServerOutputChannelSetting{}, &ModelError{
			Message: "Failed to update ServerOutputChannelSetting",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t.FindByID(ctx, gdb, id)
}

// Delete deletes a record by ID
func (t *ServerOutputChannelSetting) Delete(ctx context.Context, gdb *gorm.DB, id uint) (int64, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Delete(&ServerOutputChannelSetting{}, id)
	if db.Error != nil {
		return 0, &ModelError{
			Message: "Failed to delete ServerOutputChannelSetting",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return db.RowsAffected, nil
}
