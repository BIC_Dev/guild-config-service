package models

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// ServerOutputChannel struct
type ServerOutputChannel struct {
	ID                          uint                          `gorm:"primary_key;auto_increment" json:"id"`
	ChannelID                   string                        `gorm:"varchar(20);index" json:"channel_id"`
	OutputChannelTypeID         *string                       `gorm:"varchar(10)" json:"output_channel_type_id"`
	ServerID                    *uint                         `json:"server_id"`
	Enabled                     bool                          `json:"enabled"`
	CreatedAt                   time.Time                     `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt                   time.Time                     `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	OutputChannelType           *OutputChannelType            `gorm:"constraint:OnDelete:CASCADE" json:"output_channel_type,omitempty"`
	Server                      *Server                       `gorm:"constraint:OnDelete:CASCADE" json:"server,omitempty"`
	ServerOutputChannelSettings []*ServerOutputChannelSetting `json:"server_output_channel_settings,omitempty"`
}

// TableName func
func (ServerOutputChannel) TableName() string {
	return "server_output_channels"
}

// Create adds a record to DB
func (t *ServerOutputChannel) Create(ctx context.Context, gdb *gorm.DB) (*ServerOutputChannel, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Create(&t)
	if db.Error != nil {
		return nil, &ModelError{
			Message: "Unable to create ServerOutputChannel entry in DB",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindAll finds all records in DB
func (t *ServerOutputChannel) FindAll(ctx context.Context, gdb *gorm.DB) (*[]ServerOutputChannel, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	tables := []ServerOutputChannel{}
	db := gdb.WithContext(ctx).Find(&tables)
	if db.Error != nil {
		return &[]ServerOutputChannel{}, &ModelError{
			Message: "Failed to find all ServerOutputChannels",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &tables, nil
}

// FindByID finds a record by ID
func (t *ServerOutputChannel) FindByID(ctx context.Context, gdb *gorm.DB, id uint) (*ServerOutputChannel, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "server_output_channels.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &ServerOutputChannel{}, &ModelError{
			Message: "OutputChannel not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &ServerOutputChannel{}, &ModelError{
			Message: "Failed query to find ServerOutputChannel by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// Update updates a record by ID
func (t *ServerOutputChannel) Update(ctx context.Context, gdb *gorm.DB, id uint, params map[string]interface{}) (*ServerOutputChannel, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	params["updated_at"] = time.Now()
	db := gdb.WithContext(ctx).Model(&ServerOutputChannel{}).Where("id = ?", id).Take(&ServerOutputChannel{}).UpdateColumns(params)
	if db.Error != nil {
		return &ServerOutputChannel{}, &ModelError{
			Message: "Failed to update OutputChannel",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t.FindByID(ctx, gdb, id)
}

// Delete deletes a record by ID
func (t *ServerOutputChannel) Delete(ctx context.Context, gdb *gorm.DB, id uint) (int64, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Delete(&ServerOutputChannel{}, id)
	if db.Error != nil {
		return 0, &ModelError{
			Message: "Failed to delete ServerOutputChannel",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return db.RowsAffected, nil
}
