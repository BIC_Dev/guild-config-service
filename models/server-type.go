package models

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// ServerType model
type ServerType struct {
	ID          string    `gorm:"primary_key;varchar(20)" json:"id"`
	Description string    `gorm:"varchar(250)" json:"description"`
	Enabled     bool      `json:"enabled"`
	CreatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

// TableName func
func (ServerType) TableName() string {
	return "server_types"
}

// Create adds a record to DB
func (t *ServerType) Create(ctx context.Context, gdb *gorm.DB) (*ServerType, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Create(&t)
	if db.Error != nil {
		return nil, &ModelError{
			Message: "Unable to create ServerType entry in DB",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindAll finds all records in DB
func (t *ServerType) FindAll(ctx context.Context, gdb *gorm.DB) (*[]ServerType, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	serverTypes := []ServerType{}
	db := gdb.WithContext(ctx).Find(&serverTypes)
	if db.Error != nil {
		return &[]ServerType{}, &ModelError{
			Message: "Failed to find all Server Types",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &serverTypes, nil
}

// FindByID finds a record by ID
func (t *ServerType) FindByID(ctx context.Context, gdb *gorm.DB, id string) (*ServerType, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "server_types.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &ServerType{}, &ModelError{
			Message: "ServerType not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &ServerType{}, &ModelError{
			Message: "Failed query to find ServerType by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// Update updates a record by ID
func (t *ServerType) Update(ctx context.Context, gdb *gorm.DB, id string, params map[string]interface{}) (*ServerType, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Model(&ServerType{}).Where("id = ?", id).Take(&ServerType{}).UpdateColumns(params)
	if db.Error != nil {
		return &ServerType{}, &ModelError{
			Message: "Failed to update ServerType",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t.FindByID(ctx, gdb, id)
}

// Delete deletes a record by ID
func (t *ServerType) Delete(ctx context.Context, gdb *gorm.DB, id string) (int64, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Delete(&ServerType{}, id)
	if db.Error != nil {
		return 0, &ModelError{
			Message: "Failed to delete ServerType",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return db.RowsAffected, nil
}
