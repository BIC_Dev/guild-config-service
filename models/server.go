package models

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// Server model
type Server struct {
	ID                   uint                   `gorm:"primary_key;auto_increment" json:"id"`
	NitradoID            int                    `json:"nitrado_id"`
	Name                 string                 `gorm:"varchar(100)" json:"name"`
	ServerTypeID         *string                `gorm:"varchar(20)" json:"server_type_id"`
	GuildID              *string                `gorm:"varchar(20)" json:"guild_id"`
	NitradoTokenID       *uint                  `json:"nitrado_token_id"`
	BoostSettingID       *uint                  `json:"boost_setting_id"`
	Enabled              bool                   `json:"enabled"`
	CreatedAt            time.Time              `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt            time.Time              `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	ServerType           *ServerType            `gorm:"constraint:OnDelete:CASCADE" json:"server_type,omitempty"`
	BoostSetting         *BoostSetting          `gorm:"constraint:OnDelete:SET NULL" json:"boost_setting,omitempty"`
	NitradoToken         *NitradoToken          `gorm:"constraint:OnDelete:CASCADE" json:"nitrado_token,omitempty"`
	Guild                *Guild                 `gorm:"constraint:OnDelete:CASCADE" json:"guild,omitempty"`
	ServerOutputChannels []*ServerOutputChannel `json:"server_output_channels,omitempty"`
}

// TableName func
func (Server) TableName() string {
	return "servers"
}

// Create adds a record to DB
func (t *Server) Create(ctx context.Context, gdb *gorm.DB) (*Server, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Create(&t)
	if db.Error != nil {
		return nil, &ModelError{
			Message: "Unable to create Server entry in DB",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindAll finds all records in DB
func (t *Server) FindAll(ctx context.Context, gdb *gorm.DB) (*[]Server, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	servers := []Server{}
	db := gdb.WithContext(ctx).Find(&servers)
	if db.Error != nil {
		return &[]Server{}, &ModelError{
			Message: "Failed to find all Servers",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &servers, nil
}

// FindByID finds a record by ID
func (t *Server) FindByID(ctx context.Context, gdb *gorm.DB, id uint) (*Server, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "servers.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &Server{}, &ModelError{
			Message: "Server not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &Server{}, &ModelError{
			Message: "Failed query to find Server by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindByGuild finds a record by ID
func (t *Server) FindByGuild(ctx context.Context, gdb *gorm.DB, guildID string) (*[]Server, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	var servers []Server
	db := gdb.WithContext(ctx)
	db = db.Find(&servers, "servers.guild_id = ?", guildID)
	if db.Error == gorm.ErrRecordNotFound {
		return &[]Server{}, &ModelError{
			Message: "Servers not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &[]Server{}, &ModelError{
			Message: "Failed query to find Servers by GuildID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &servers, nil
}

// Update updates a record by ID
func (t *Server) Update(ctx context.Context, gdb *gorm.DB, id uint, params map[string]interface{}) (*Server, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	params["updated_at"] = time.Now()
	db := gdb.WithContext(ctx).Model(&Server{}).Where("id = ?", id).Take(&Server{}).UpdateColumns(params)
	if db.Error != nil {
		return &Server{}, &ModelError{
			Message: "Failed to update Server",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t.FindByID(ctx, gdb, id)
}

// Delete deletes a record by ID
func (t *Server) Delete(ctx context.Context, gdb *gorm.DB, id uint) (int64, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Delete(&Server{}, id)
	if db.Error != nil {
		return 0, &ModelError{
			Message: "Failed to delete Server",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return db.RowsAffected, nil
}
