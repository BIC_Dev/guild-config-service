package models

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// Setup model
type Setup struct {
	ID             uint          `gorm:"primary_key;" json:"id"`
	GuildID        *string       `gorm:"varchar(20)" json:"guild_id"`
	ContactID      *string       `gorm:"varchar(20)" json:"contact_id"`
	GuildServiceID *uint         `json:"guild_service_id"`
	CreatedAt      time.Time     `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt      time.Time     `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	Contact        *Contact      `gorm:"constraint:OnDelete:SET NULL" json:"contact,omitempty"`
	Guild          *Guild        `gorm:"constraint:OnDelete:CASCADE" json:"guild,omitempty"`
	GuildService   *GuildService `gorm:"constraint:OnDelete:SET NULL" json:"guild_service,omitempty"`
}

// TableName func
func (Setup) TableName() string {
	return "setups"
}

// Create adds a record to DB
func (t *Setup) Create(ctx context.Context, gdb *gorm.DB) (*Setup, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Create(&t)
	if db.Error != nil {
		return nil, &ModelError{
			Message: "Unable to create Setup entry in DB",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// FindAll finds all records in DB
func (t *Setup) FindAll(ctx context.Context, gdb *gorm.DB) (*[]Setup, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	nitradoTokens := []Setup{}
	db := gdb.WithContext(ctx).Find(&nitradoTokens)
	if db.Error != nil {
		return &[]Setup{}, &ModelError{
			Message: "Failed to find all Setups",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &nitradoTokens, nil
}

// FindByGuild finds all records in DB
func (t *Setup) FindByGuild(ctx context.Context, gdb *gorm.DB, guildID string) (*[]Setup, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	nitradoTokens := []Setup{}
	db := gdb.WithContext(ctx).Find(&nitradoTokens, "setups.guild_id = ?", guildID)
	if db.Error != nil {
		return &[]Setup{}, &ModelError{
			Message: "Failed to find all Setups by Guild",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return &nitradoTokens, nil
}

// FindByID finds a record by ID
func (t *Setup) FindByID(ctx context.Context, gdb *gorm.DB, id uint) (*Setup, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx)
	db = db.Take(&t, "setups.id = ?", id)
	if db.Error == gorm.ErrRecordNotFound {
		return &Setup{}, &ModelError{
			Message: "Setup not found",
			Err:     db.Error,
		}
	}
	if db.Error != nil {
		return &Setup{}, &ModelError{
			Message: "Failed query to find Setup by ID",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t, nil
}

// Update updates a record by ID
func (t *Setup) Update(ctx context.Context, gdb *gorm.DB, id uint, params map[string]interface{}) (*Setup, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Model(&Setup{}).Where("id = ?", id).Take(&Setup{}).UpdateColumns(params)
	if db.Error != nil {
		return &Setup{}, &ModelError{
			Message: "Failed to update Setup",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return t.FindByID(ctx, gdb, id)
}

// Delete deletes a record by ID
func (t *Setup) Delete(ctx context.Context, gdb *gorm.DB, id uint) (int64, *ModelError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	db := gdb.WithContext(ctx).Delete(&Setup{}, id)
	if db.Error != nil {
		return 0, &ModelError{
			Message: "Failed to delete Setup",
			Err:     db.Error,
		}
	}

	ctx = logging.AddValues(ctx, zap.Int64("rows_affected", db.RowsAffected))

	return db.RowsAffected, nil
}
