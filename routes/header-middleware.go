package routes

import (
	"errors"
	"net/http"

	"gitlab.com/BIC_Dev/guild-config-service/configs"
	"gitlab.com/BIC_Dev/guild-config-service/controllers"
	"gitlab.com/BIC_Dev/guild-config-service/utils/cache"
	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// Header struct
type Header struct {
	BasePath string
	DB       *gorm.DB
	Cache    *cache.Cache
	Config   *configs.Config
}

// HeaderMiddleware verifies the required headers are set
func (m Header) HeaderMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

		switch r.Method {
		case "GET":
			switch r.URL.Path[len(m.BasePath):] {
			case "/status",
				"/boost-settings",
				"/contacts",
				"/guild-service-contacts",
				"/guild-service-permissions",
				"/guild-services",
				"/guilds",
				"/nitrado-tokens",
				"/output-channel-types",
				"/server-output-channels",
				"/server-output-channel-settings",
				"/guild-output-channels",
				"/server-types",
				"/servers",
				"/setups",
				"/ark-tribes",
				"/ark-tribe-members":
				next.ServeHTTP(w, r)
				return
			}
		case "POST":
			switch r.URL.Path[len(m.BasePath):] {
			case "/guilds", "/output-channel-types", "/server-types":
				next.ServeHTTP(w, r)
				return
			}
		case "PUT":
		case "DELETE":
			break
		}

		guildHeader := r.Header.Get("Guild")

		if guildHeader == "" {
			controllers.Error(ctx, w, "A Guild header must be set for all routes", errors.New("Missing Guild header"), http.StatusBadRequest)
			return
		}

		// switch r.Method {
		// case "POST":
		// 	switch r.URL.Path[len(m.BasePath):] {
		// 	case "/nitrado-setups", "/guilds", "/nitrado-tokens", "/guild-services":
		// 		next.ServeHTTP(w, r)
		// 		return
		// 	}
		// case "GET":
		// case "PUT":
		// case "DELETE":
		// 	break
		// }

		// guild := models.Guild{}

		// cgErr := controllers.GetCache(ctx, m.Cache, m.Config.CacheSettings.Guild, guildHeader, &guild)
		// if cgErr != nil {
		// 	controllers.Error(ctx, w, cgErr.Message, cgErr.Err, http.StatusInternalServerError)
		// 	return
		// }

		// if guild.ID != "" {
		// 	next.ServeHTTP(w, r)
		// 	return
		// }

		// aGuild, agErr := guild.FindByID(ctx, m.DB, guildHeader)
		// if agErr != nil {
		// 	controllers.Error(ctx, w, "Invalid Guild header", agErr.Err, http.StatusBadRequest)
		// 	return
		// }

		// sgErr := controllers.SetCache(ctx, m.Cache, m.Config.CacheSettings.Guild, guildHeader, aGuild)
		// if sgErr != nil {
		// 	controllers.Error(ctx, w, sgErr.Message, sgErr.Err, http.StatusInternalServerError)
		// 	return
		// }

		next.ServeHTTP(w, r)
	})
}
