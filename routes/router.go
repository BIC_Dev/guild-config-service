package routes

import (
	"context"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/guild-config-service/controllers"
	"gitlab.com/BIC_Dev/guild-config-service/utils/logging"
	"go.uber.org/zap"
)

// Router struct
type Router struct {
	Controller   *controllers.Controller
	ServiceToken string
	Port         string
	BasePath     string
}

// GetRouter creates and returns a router
func GetRouter(ctx context.Context) *mux.Router {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))
	return mux.NewRouter().StrictSlash(true)
}

// AddRoutes adds all necessary routes to the router
func AddRoutes(ctx context.Context, router *mux.Router, r Router) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))
	auth := Authentication{
		ServiceToken: r.ServiceToken,
		BasePath:     r.BasePath,
	}

	headers := Header{
		BasePath: r.BasePath,
		DB:       r.Controller.DB,
		Cache:    r.Controller.Cache,
		Config:   r.Controller.Config,
	}

	// STATUS
	router.HandleFunc(r.BasePath+"/status", r.Controller.GetStatus).Methods("GET")

	// GUILD FEEDS
	router.HandleFunc(r.BasePath+"/guild-feeds/{guild_id}", r.Controller.GetGuildFeedByID).Methods("GET")

	// CONTACTS
	router.HandleFunc(r.BasePath+"/contacts", r.Controller.GetAllContacts).Methods("GET")
	router.HandleFunc(r.BasePath+"/contacts/{contact_id}", r.Controller.GetContactByID).Methods("GET")
	router.HandleFunc(r.BasePath+"/contacts", r.Controller.CreateContact).Methods("POST")
	router.HandleFunc(r.BasePath+"/contacts/{contact_id}", r.Controller.UpdateContact).Methods("PUT")
	router.HandleFunc(r.BasePath+"/contacts/{contact_id}", r.Controller.DeleteContact).Methods("DELETE")

	// OUTPUT CHANNEL TYPES
	router.HandleFunc(r.BasePath+"/output-channel-types", r.Controller.GetAllOutputChannelTypes).Methods("GET")
	router.HandleFunc(r.BasePath+"/output-channel-types/{output_channel_type_id}", r.Controller.GetOutputChannelTypeByID).Methods("GET")
	router.HandleFunc(r.BasePath+"/output-channel-types", r.Controller.CreateOutputChannelType).Methods("POST")
	router.HandleFunc(r.BasePath+"/output-channel-types/{output_channel_type_id}", r.Controller.UpdateOutputChannelType).Methods("PUT")
	router.HandleFunc(r.BasePath+"/output-channel-types/{output_channel_type_id}", r.Controller.DeleteOutputChannelType).Methods("DELETE")

	// SERVER TYPES
	router.HandleFunc(r.BasePath+"/server-types", r.Controller.GetAllServerTypes).Methods("GET")
	router.HandleFunc(r.BasePath+"/server-types/{server_type_id}", r.Controller.GetServerTypeByID).Methods("GET")
	router.HandleFunc(r.BasePath+"/server-types", r.Controller.CreateServerType).Methods("POST")
	router.HandleFunc(r.BasePath+"/server-types/{server_type_id}", r.Controller.UpdateServerType).Methods("PUT")
	router.HandleFunc(r.BasePath+"/server-types/{server_type_id}", r.Controller.DeleteServerType).Methods("DELETE")

	// GUILDS
	router.HandleFunc(r.BasePath+"/guilds", r.Controller.GetAllGuilds).Methods("GET")
	router.HandleFunc(r.BasePath+"/guilds/{guild_id}", r.Controller.GetGuildByID).Methods("GET")
	router.HandleFunc(r.BasePath+"/guilds", r.Controller.CreateGuild).Methods("POST")
	router.HandleFunc(r.BasePath+"/guilds/{guild_id}", r.Controller.UpdateGuild).Methods("PUT")
	router.HandleFunc(r.BasePath+"/guilds/{guild_id}", r.Controller.DeleteGuild).Methods("DELETE")

	// SERVERS
	router.HandleFunc(r.BasePath+"/servers", r.Controller.GetAllServers).Methods("GET")
	router.HandleFunc(r.BasePath+"/servers/{server_id}", r.Controller.GetServerByID).Methods("GET")
	router.HandleFunc(r.BasePath+"/servers", r.Controller.CreateServer).Methods("POST")
	router.HandleFunc(r.BasePath+"/servers/{server_id}", r.Controller.UpdateServer).Methods("PUT")
	router.HandleFunc(r.BasePath+"/servers/{server_id}", r.Controller.DeleteServer).Methods("DELETE")

	// NITRADO TOKENS
	router.HandleFunc(r.BasePath+"/nitrado-tokens", r.Controller.GetAllNitradoTokens).Methods("GET")
	router.HandleFunc(r.BasePath+"/nitrado-tokens/{nitrado_token_id}", r.Controller.GetNitradoTokenByID).Methods("GET")
	router.HandleFunc(r.BasePath+"/nitrado-tokens", r.Controller.CreateNitradoToken).Methods("POST")
	router.HandleFunc(r.BasePath+"/nitrado-tokens/{nitrado_token_id}", r.Controller.UpdateNitradoToken).Methods("PUT")
	router.HandleFunc(r.BasePath+"/nitrado-tokens/{nitrado_token_id}", r.Controller.DeleteNitradoToken).Methods("DELETE")

	// GUILD SERVICES
	router.HandleFunc(r.BasePath+"/guild-services", r.Controller.GetAllGuildServices).Methods("GET")
	router.HandleFunc(r.BasePath+"/guild-services/{guild_service_id}", r.Controller.GetGuildServiceByID).Methods("GET")
	router.HandleFunc(r.BasePath+"/guild-services", r.Controller.CreateGuildService).Methods("POST")
	router.HandleFunc(r.BasePath+"/guild-services/{guild_service_id}", r.Controller.UpdateGuildService).Methods("PUT")
	router.HandleFunc(r.BasePath+"/guild-services/{guild_service_id}", r.Controller.DeleteGuildService).Methods("DELETE")

	// SETUPS
	router.HandleFunc(r.BasePath+"/setups", r.Controller.GetAllSetups).Methods("GET")
	router.HandleFunc(r.BasePath+"/setups/{setup_id}", r.Controller.GetSetupByID).Methods("GET")
	router.HandleFunc(r.BasePath+"/setups", r.Controller.CreateSetup).Methods("POST")
	router.HandleFunc(r.BasePath+"/setups/{setup_id}", r.Controller.UpdateSetup).Methods("PUT")
	router.HandleFunc(r.BasePath+"/setups/{setup_id}", r.Controller.DeleteSetup).Methods("DELETE")

	// BOOST SETTINGS
	router.HandleFunc(r.BasePath+"/boost-settings", r.Controller.GetAllBoostSettings).Methods("GET")
	router.HandleFunc(r.BasePath+"/boost-settings/{boost_setting_id}", r.Controller.GetBoostSettingByID).Methods("GET")
	router.HandleFunc(r.BasePath+"/boost-settings", r.Controller.CreateBoostSetting).Methods("POST")
	router.HandleFunc(r.BasePath+"/boost-settings/{boost_setting_id}", r.Controller.UpdateBoostSetting).Methods("PUT")
	router.HandleFunc(r.BasePath+"/boost-settings/{boost_setting_id}", r.Controller.DeleteBoostSetting).Methods("DELETE")

	// GUILD SERVICE CONTACTS
	router.HandleFunc(r.BasePath+"/guild-service-contacts", r.Controller.GetAllGuildServiceContacts).Methods("GET")
	router.HandleFunc(r.BasePath+"/guild-service-contacts/{guild_service_contact_id}", r.Controller.GetGuildServiceContactByID).Methods("GET")
	router.HandleFunc(r.BasePath+"/guild-service-contacts", r.Controller.CreateGuildServiceContact).Methods("POST")
	router.HandleFunc(r.BasePath+"/guild-service-contacts/{guild_service_contact_id}", r.Controller.UpdateGuildServiceContact).Methods("PUT")
	router.HandleFunc(r.BasePath+"/guild-service-contacts/{guild_service_contact_id}", r.Controller.DeleteGuildServiceContact).Methods("DELETE")

	// GUILD SERVICE PERMISSIONS
	router.HandleFunc(r.BasePath+"/guild-service-permissions", r.Controller.GetAllGuildServicePermissions).Methods("GET")
	router.HandleFunc(r.BasePath+"/guild-service-permissions/{guild_service_permission_id}", r.Controller.GetGuildServicePermissionByID).Methods("GET")
	router.HandleFunc(r.BasePath+"/guild-service-permissions", r.Controller.CreateGuildServicePermission).Methods("POST")
	router.HandleFunc(r.BasePath+"/guild-service-permissions/{guild_service_permission_id}", r.Controller.UpdateGuildServicePermission).Methods("PUT")
	router.HandleFunc(r.BasePath+"/guild-service-permissions/{guild_service_permission_id}", r.Controller.DeleteGuildServicePermission).Methods("DELETE")

	// SERVER OUTPUT CHANNELS
	router.HandleFunc(r.BasePath+"/server-output-channels", r.Controller.GetAllServerOutputChannels).Methods("GET")
	router.HandleFunc(r.BasePath+"/server-output-channels/{server_output_channel_id}", r.Controller.GetServerOutputChannelByID).Methods("GET")
	router.HandleFunc(r.BasePath+"/server-output-channels", r.Controller.CreateServerOutputChannel).Methods("POST")
	router.HandleFunc(r.BasePath+"/server-output-channels/{server_output_channel_id}", r.Controller.UpdateServerOutputChannel).Methods("PUT")
	router.HandleFunc(r.BasePath+"/server-output-channels/{server_output_channel_id}", r.Controller.DeleteServerOutputChannel).Methods("DELETE")

	// SERVER OUTPUT CHANNEL SETTINGS
	router.HandleFunc(r.BasePath+"/server-output-channel-settings", r.Controller.GetAllServerOutputChannelSettings).Methods("GET")
	router.HandleFunc(r.BasePath+"/server-output-channel-settings/{server_output_channel_setting_id}", r.Controller.GetServerOutputChannelSettingByID).Methods("GET")
	router.HandleFunc(r.BasePath+"/server-output-channel-settings", r.Controller.CreateServerOutputChannelSetting).Methods("POST")
	router.HandleFunc(r.BasePath+"/server-output-channel-settings/{server_output_channel_setting_id}", r.Controller.UpdateServerOutputChannelSetting).Methods("PUT")
	router.HandleFunc(r.BasePath+"/server-output-channel-settings/{server_output_channel_setting_id}", r.Controller.DeleteServerOutputChannelSetting).Methods("DELETE")

	// OUTPUT CHANNELS
	router.HandleFunc(r.BasePath+"/guild-output-channels", r.Controller.GetAllGuildOutputChannels).Methods("GET")
	router.HandleFunc(r.BasePath+"/guild-output-channels/{guild_output_channel_id}", r.Controller.GetGuildOutputChannelByID).Methods("GET")
	router.HandleFunc(r.BasePath+"/guild-output-channels", r.Controller.CreateGuildOutputChannel).Methods("POST")
	router.HandleFunc(r.BasePath+"/guild-output-channels/{guild_output_channel_id}", r.Controller.UpdateGuildOutputChannel).Methods("PUT")
	router.HandleFunc(r.BasePath+"/guild-output-channels/{guild_output_channel_id}", r.Controller.DeleteGuildOutputChannel).Methods("DELETE")

	// Nitrado Setup
	router.HandleFunc(r.BasePath+"/nitrado-setups", r.Controller.CreateNitradoSetup).Methods("POST")

	// ARK TRIBES
	router.HandleFunc(r.BasePath+"/ark-tribes", r.Controller.GetAllArkTribes).Methods("GET")
	router.HandleFunc(r.BasePath+"/ark-tribes/{ark_tribe_id}", r.Controller.GetArkTribeByID).Methods("GET")
	router.HandleFunc(r.BasePath+"/ark-tribes", r.Controller.CreateArkTribe).Methods("POST")
	router.HandleFunc(r.BasePath+"/ark-tribes/{ark_tribe_id}", r.Controller.UpdateArkTribe).Methods("PUT")
	router.HandleFunc(r.BasePath+"/ark-tribes/{ark_tribe_id}", r.Controller.DeleteArkTribe).Methods("DELETE")

	// ARK TRIBE MEMBERS
	router.HandleFunc(r.BasePath+"/ark-tribe-members", r.Controller.GetAllArkTribeMembers).Methods("GET")
	router.HandleFunc(r.BasePath+"/ark-tribe-members/{ark_tribe_member_id}", r.Controller.GetArkTribeMemberByID).Methods("GET")
	router.HandleFunc(r.BasePath+"/ark-tribe-members", r.Controller.CreateArkTribeMember).Methods("POST")
	router.HandleFunc(r.BasePath+"/ark-tribe-members/{ark_tribe_member_id}", r.Controller.UpdateArkTribeMember).Methods("PUT")
	router.HandleFunc(r.BasePath+"/ark-tribe-members/{ark_tribe_member_id}", r.Controller.DeleteArkTribeMember).Methods("DELETE")

	router.Use(auth.AuthenticationMiddleware)
	router.Use(headers.HeaderMiddleware)

	loggingMiddleware := LoggingMiddleware()
	loggedRouter := loggingMiddleware(router)

	logger := logging.Logger(ctx)
	logger.Info("Starting Listener", zap.String("port", r.Port))
	if err := http.ListenAndServe(":"+r.Port, loggedRouter); err != nil {
		logger.Fatal("error_log", zap.NamedError("err", err))
	}
}
