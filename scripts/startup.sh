export ENVIRONMENT=$(echo ${ENV_VARS} | jq -r '.ENVIRONMENT')
export SERVICE_TOKEN=$(echo ${ENV_VARS} | jq -r '.SERVICE_TOKEN')
export LOG_LEVEL=$(echo ${ENV_VARS} | jq -r '.LOG_LEVEL')
export BASE_PATH=$(echo ${ENV_VARS} | jq -r '.BASE_PATH')
export NITRADO_SERVICE_TOKEN=$(echo ${ENV_VARS} | jq -r '.NITRADO_SERVICE_TOKEN')
export MIGRATE=$(echo ${ENV_VARS} | jq -r '.MIGRATE')
export POSTGRES_USERNAME=$(echo ${ENV_VARS} | jq -r '.POSTGRES_USERNAME')
export POSTGRES_PASSWORD=$(echo ${ENV_VARS} | jq -r '.POSTGRES_PASSWORD')

/main