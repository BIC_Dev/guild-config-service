package db

// ModelError model error struct
type ModelError struct {
	Err     error
	Message string
}

func (r *ModelError) Error() string {
	return r.Err.Error()
}

// GetMessage details of the error
func (r *ModelError) GetMessage() string {
	return r.Message
}

// SetMessage sets the details map of the error
func (r *ModelError) SetMessage(message string) {
	r.Message = message
}
