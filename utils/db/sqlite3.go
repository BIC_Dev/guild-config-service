package db

import (
	"fmt"

	"gitlab.com/BIC_Dev/guild-config-service/configs"
	"gorm.io/gorm"

	// Loading SQLite3 dialect
	"gorm.io/driver/sqlite"
	_ "gorm.io/driver/sqlite"
)

// SQLite3 struct
type SQLite3 struct {
	DB *gorm.DB
}

// Connect starts initial connection to DB
func (sql3 *SQLite3) Connect(c *configs.Config) *ModelError {
	dbPath := fmt.Sprintf("%s%s", c.SQLite3.Path, c.SQLite3.DBName)
	logger := NewLogger(0, 0)
	newDB, err := gorm.Open(sqlite.Open(dbPath), &gorm.Config{
		Logger: logger,
	})

	if err != nil {
		if err != nil {
			return &ModelError{
				Err:     err,
				Message: "Failed to connect to SQLite3 DB",
			}
		}
	}

	sql3.DB = newDB

	return nil
}

// Migrate migrates a table
func (sql3 *SQLite3) Migrate(table interface{}) *ModelError {
	sql3.DB.AutoMigrate(table)

	return nil
}

// GetDB gets the DB from the struct
func (sql3 *SQLite3) GetDB() *gorm.DB {
	return sql3.DB
}
