package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetAllArkTribeMembersResponse struct
type GetAllArkTribeMembersResponse struct {
	Message         string                  `json:"message"`
	ArkTribeMembers []models.ArkTribeMember `json:"ark_tribe_members"`
}

// GetArkTribeMemberByIDResponse struct
type GetArkTribeMemberByIDResponse struct {
	Message        string                `json:"message"`
	ArkTribeMember models.ArkTribeMember `json:"ark_tribe_member"`
}

// CreateArkTribeMemberResponse struct
type CreateArkTribeMemberResponse struct {
	Message        string                `json:"message"`
	ArkTribeMember models.ArkTribeMember `json:"ark_tribe_member"`
}

// UpdateArkTribeMemberRequest struct
type UpdateArkTribeMemberRequest struct {
	MemberID   string `json:"member_id"`
	ArkTribeID uint   `json:"ark_tribe_id"`
	Enabled    bool   `json:"enabled"`
}

// UpdateArkTribeMemberResponse struct
type UpdateArkTribeMemberResponse struct {
	Message        string                `json:"message"`
	ArkTribeMember models.ArkTribeMember `json:"ark_tribe_member"`
}

// DeleteArkTribeMemberResponse struct
type DeleteArkTribeMemberResponse struct {
	Message string `json:"message"`
	Count   int64  `json:"count"`
}
