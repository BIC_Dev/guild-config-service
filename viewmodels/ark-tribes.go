package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetAllArkTribesResponse struct
type GetAllArkTribesResponse struct {
	Message   string            `json:"message"`
	ArkTribes []models.ArkTribe `json:"ark_tribes"`
}

// GetArkTribeByIDResponse struct
type GetArkTribeByIDResponse struct {
	Message  string          `json:"message"`
	ArkTribe models.ArkTribe `json:"ark_tribe"`
}

// CreateArkTribeResponse struct
type CreateArkTribeResponse struct {
	Message  string          `json:"message"`
	ArkTribe models.ArkTribe `json:"ark_tribe"`
}

// UpdateArkTribeRequest struct
type UpdateArkTribeRequest struct {
	Name     string               `json:"name"`
	TribeIDs models.ArkTribeJSONB `json:"tribe_ids"`
	LeaderID string               `json:"leader_id"`
	GuildID  string               `json:"guild_id"`
	Enabled  bool                 `json:"enabled"`
}

// UpdateArkTribeResponse struct
type UpdateArkTribeResponse struct {
	Message  string          `json:"message"`
	ArkTribe models.ArkTribe `json:"ark_tribe"`
}

// DeleteArkTribeResponse struct
type DeleteArkTribeResponse struct {
	Message string `json:"message"`
	Count   int64  `json:"count"`
}
