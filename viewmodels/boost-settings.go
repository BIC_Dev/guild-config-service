package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetAllBoostSettingsResponse struct
type GetAllBoostSettingsResponse struct {
	Message       string                `json:"message"`
	BoostSettings []models.BoostSetting `json:"boost_settings"`
}

// GetBoostSettingByIDResponse struct
type GetBoostSettingByIDResponse struct {
	Message      string              `json:"message"`
	BoostSetting models.BoostSetting `json:"boost_setting"`
}

// CreateBoostSettingResponse struct
type CreateBoostSettingResponse struct {
	Message      string              `json:"message"`
	BoostSetting models.BoostSetting `json:"boost_setting"`
}

// UpdateBoostSettingRequest struct
type UpdateBoostSettingRequest struct {
	Code           string `json:"code"`
	Message        string `json:"message"`
	WelcomeMessage string `json:"welcome_message"`
	BoostEnabled   bool   `json:"boost_enabled"`
	Enabled        bool   `json:"enabled"`
}

// UpdateBoostSettingResponse struct
type UpdateBoostSettingResponse struct {
	Message      string              `json:"message"`
	BoostSetting models.BoostSetting `json:"boost_setting"`
}

// DeleteBoostSettingResponse struct
type DeleteBoostSettingResponse struct {
	Message string `json:"message"`
	Count   int64  `json:"count"`
}
