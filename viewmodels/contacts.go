package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetAllContactsResponse struct
type GetAllContactsResponse struct {
	Message  string           `json:"message"`
	Contacts []models.Contact `json:"contacts"`
}

// GetContactByIDResponse struct
type GetContactByIDResponse struct {
	Message string         `json:"message"`
	Contact models.Contact `json:"contact"`
}

// CreateContactResponse struct
type CreateContactResponse struct {
	Message string         `json:"message"`
	Contact models.Contact `json:"contact"`
}

// UpdateContactRequest struct
type UpdateContactRequest struct {
	Name    string `json:"name"`
	Enabled bool   `json:"enabled"`
}

// UpdateContactResponse struct
type UpdateContactResponse struct {
	Message string         `json:"message"`
	Contact models.Contact `json:"contact"`
}

// DeleteContactResponse struct
type DeleteContactResponse struct {
	Message string `json:"message"`
	Count   int64  `json:"count"`
}
