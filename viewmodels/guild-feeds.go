package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetGuildFeedByIDResponse struct
type GetGuildFeedByIDResponse struct {
	Message string       `json:"message"`
	Guild   models.Guild `json:"guild"`
}
