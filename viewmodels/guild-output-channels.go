package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetAllGuildOutputChannelsResponse struct
type GetAllGuildOutputChannelsResponse struct {
	Message             string                      `json:"message"`
	GuildOutputChannels []models.GuildOutputChannel `json:"guild_output_channels"`
}

// GetGuildOutputChannelByIDResponse struct
type GetGuildOutputChannelByIDResponse struct {
	Message            string                    `json:"message"`
	GuildOutputChannel models.GuildOutputChannel `json:"guild_output_channel"`
}

// CreateGuildOutputChannelResponse struct
type CreateGuildOutputChannelResponse struct {
	Message            string                    `json:"message"`
	GuildOutputChannel models.GuildOutputChannel `json:"guild_output_channel"`
}

// UpdateGuildOutputChannelRequest struct
type UpdateGuildOutputChannelRequest struct {
	ChannelID           string `json:"channel_id"`
	OutputChannelTypeID string `json:"output_channel_type_id"`
	GuildID             string `json:"guild_id"`
	Enabled             bool   `json:"enabled"`
}

// UpdateGuildOutputChannelResponse struct
type UpdateGuildOutputChannelResponse struct {
	Message            string                    `json:"message"`
	GuildOutputChannel models.GuildOutputChannel `json:"guild_output_channel"`
}

// DeleteGuildOutputChannelResponse struct
type DeleteGuildOutputChannelResponse struct {
	Message string `json:"message"`
	Count   int64  `json:"count"`
}
