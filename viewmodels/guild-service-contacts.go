package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetAllGuildServiceContactsResponse struct
type GetAllGuildServiceContactsResponse struct {
	Message              string                       `json:"message"`
	GuildServiceContacts []models.GuildServiceContact `json:"guild_service_contacts"`
}

// GetGuildServiceContactByIDResponse struct
type GetGuildServiceContactByIDResponse struct {
	Message             string                     `json:"message"`
	GuildServiceContact models.GuildServiceContact `json:"guild_service_contact"`
}

// CreateGuildServiceContactResponse struct
type CreateGuildServiceContactResponse struct {
	Message             string                     `json:"message"`
	GuildServiceContact models.GuildServiceContact `json:"guild_service_contact"`
}

// UpdateGuildServiceContactRequest struct
type UpdateGuildServiceContactRequest struct {
	GuildServiceID uint   `json:"guild_service_id"`
	ContactID      string `json:"contact_id"`
}

// UpdateGuildServiceContactResponse struct
type UpdateGuildServiceContactResponse struct {
	Message             string                     `json:"message"`
	GuildServiceContact models.GuildServiceContact `json:"guild_service_contact"`
}

// DeleteGuildServiceContactResponse struct
type DeleteGuildServiceContactResponse struct {
	Message string `json:"message"`
	Count   int64  `json:"count"`
}
