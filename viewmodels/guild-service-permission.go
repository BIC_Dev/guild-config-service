package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetAllGuildServicePermissionsResponse struct
type GetAllGuildServicePermissionsResponse struct {
	Message                 string                          `json:"message"`
	GuildServicePermissions []models.GuildServicePermission `json:"guild_service_permissions"`
}

// GetGuildServicePermissionByIDResponse struct
type GetGuildServicePermissionByIDResponse struct {
	Message                string                        `json:"message"`
	GuildServicePermission models.GuildServicePermission `json:"guild_service_permission"`
}

// CreateGuildServicePermissionResponse struct
type CreateGuildServicePermissionResponse struct {
	Message                string                        `json:"message"`
	GuildServicePermission models.GuildServicePermission `json:"guild_service_permission"`
}

// UpdateGuildServicePermissionRequest struct
type UpdateGuildServicePermissionRequest struct {
	CommandName    string `json:"command_name"`
	RoleID         string `json:"role_id"`
	GuildServiceID uint   `json:"guild_service_id"`
	Enabled        bool   `json:"enabled"`
}

// UpdateGuildServicePermissionResponse struct
type UpdateGuildServicePermissionResponse struct {
	Message                string                        `json:"message"`
	GuildServicePermission models.GuildServicePermission `json:"guild_service_permission"`
}

// DeleteGuildServicePermissionResponse struct
type DeleteGuildServicePermissionResponse struct {
	Message string `json:"message"`
	Count   int64  `json:"count"`
}
