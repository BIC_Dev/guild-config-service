package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetAllGuildServicesResponse struct
type GetAllGuildServicesResponse struct {
	Message       string                `json:"message"`
	GuildServices []models.GuildService `json:"guild_services"`
}

// GetGuildServiceByIDResponse struct
type GetGuildServiceByIDResponse struct {
	Message      string              `json:"message"`
	GuildService models.GuildService `json:"guild_service"`
}

// CreateGuildServiceResponse struct
type CreateGuildServiceResponse struct {
	Message      string              `json:"message"`
	GuildService models.GuildService `json:"guild_service"`
}

// UpdateGuildServiceRequest struct
type UpdateGuildServiceRequest struct {
	Name    string `json:"name"`
	GuildID string `json:"guild_id"`
	Enabled bool   `json:"enabled"`
}

// UpdateGuildServiceResponse struct
type UpdateGuildServiceResponse struct {
	Message      string              `json:"message"`
	GuildService models.GuildService `json:"guild_service"`
}

// DeleteGuildServiceResponse struct
type DeleteGuildServiceResponse struct {
	Message string `json:"message"`
	Count   int64  `json:"count"`
}
