package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetAllGuildsResponse struct
type GetAllGuildsResponse struct {
	Message string         `json:"message"`
	Guilds  []models.Guild `json:"guilds"`
}

// GetGuildByIDResponse struct
type GetGuildByIDResponse struct {
	Message string       `json:"message"`
	Guild   models.Guild `json:"guild"`
}

// CreateGuildRequest struct
type CreateGuildRequest struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	Enabled bool   `json:"enabled"`
}

// CreateGuildResponse struct
type CreateGuildResponse struct {
	Message string       `json:"message"`
	Guild   models.Guild `json:"guild"`
}

// UpdateGuildRequest struct
type UpdateGuildRequest struct {
	Name    string `json:"name"`
	Enabled bool   `json:"enabled"`
}

// UpdateGuildResponse struct
type UpdateGuildResponse struct {
	Message string       `json:"message"`
	Guild   models.Guild `json:"guild"`
}

// DeleteGuildResponse struct
type DeleteGuildResponse struct {
	Message string `json:"message"`
	Count   int64  `json:"count"`
}
