package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// CreateNitradoSetupRequest struct
type CreateNitradoSetupRequest struct {
	GuildID          string `json:"guild_id"`
	GuildName        string `json:"guild_name"`
	ContactID        string `json:"contact_id"`
	ContactName      string `json:"contact_name"`
	GuildServiceName string `json:"guild_service_name"`
}

// CreateNitradoSetupResponse struct
type CreateNitradoSetupResponse struct {
	Message       string                `json:"message"`
	NitradoTokens []models.NitradoToken `json:"nitrado_tokens"`
	Servers       []models.Server       `json:"servers"`
	Contact       models.Contact        `json:"contact"`
	Setup         models.Setup          `json:"setup"`
}
