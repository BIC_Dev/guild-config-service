package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetAllNitradoTokensResponse struct
type GetAllNitradoTokensResponse struct {
	Message       string                `json:"message"`
	NitradoTokens []models.NitradoToken `json:"nitrado_tokens"`
}

// GetNitradoTokenByIDResponse struct
type GetNitradoTokenByIDResponse struct {
	Message      string              `json:"message"`
	NitradoToken models.NitradoToken `json:"nitrado_token"`
}

// CreateNitradoTokenResponse struct
type CreateNitradoTokenResponse struct {
	Message      string              `json:"message"`
	NitradoToken models.NitradoToken `json:"nitrado_token"`
}

// UpdateNitradoTokenRequest struct
type UpdateNitradoTokenRequest struct {
	GuildID string `json:"guild_id"`
	Enabled bool   `json:"enabled"`
}

// UpdateNitradoTokenResponse struct
type UpdateNitradoTokenResponse struct {
	Message      string              `json:"message"`
	NitradoToken models.NitradoToken `json:"nitrado_token"`
}

// DeleteNitradoTokenResponse struct
type DeleteNitradoTokenResponse struct {
	Message string `json:"message"`
	Count   int64  `json:"count"`
}
