package viewmodels

// CreateNSMSetupRequest struct
type CreateNSMSetupRequest struct {
	GuildID string                       `json:"guild_id"`
	Options CreateNSMSetupRequestOptions `json:"options"`
}

// CreateNSMSetupRequestOptions struct
type CreateNSMSetupRequestOptions struct {
	CreateChannels  bool     `json:"create_channels"`
	NitradoAccounts []string `json:"nitrado_accounts"`
	ServerTypes     []string `json:"server_types"`
}

// CreateNSMSetupResponse struct
type CreateNSMSetupResponse struct {
	Message              string `json:"message"`
	NitradoAccountsCount int64  `json:"nitrado_accounts_count"`
	ServersCount         int64  `json:"servers_count"`
}

// DeleteNSMSetupResponse struct
type DeleteNSMSetupResponse struct {
	Message      string `json:"message"`
	ServersCount int64  `json:"servers_count"`
}
