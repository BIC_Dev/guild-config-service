package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetAllOutputChannelTypesResponse struct
type GetAllOutputChannelTypesResponse struct {
	Message            string                     `json:"message"`
	OutputChannelTypes []models.OutputChannelType `json:"output_channel_types"`
}

// GetOutputChannelTypeByIDResponse struct
type GetOutputChannelTypeByIDResponse struct {
	Message           string                   `json:"message"`
	OutputChannelType models.OutputChannelType `json:"output_channel_type"`
}

// CreateOutputChannelTypeResponse struct
type CreateOutputChannelTypeResponse struct {
	Message           string                   `json:"message"`
	OutputChannelType models.OutputChannelType `json:"output_channel_type"`
}

// UpdateOutputChannelTypeRequest struct
type UpdateOutputChannelTypeRequest struct {
	Description string `json:"description"`
	Enabled     bool   `json:"enabled"`
}

// UpdateOutputChannelTypeResponse struct
type UpdateOutputChannelTypeResponse struct {
	Message           string                   `json:"message"`
	OutputChannelType models.OutputChannelType `json:"output_channel_type"`
}

// DeleteOutputChannelTypeResponse struct
type DeleteOutputChannelTypeResponse struct {
	Message string `json:"message"`
	Count   int64  `json:"count"`
}
