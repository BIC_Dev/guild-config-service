package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetAllServerOutputChannelSettingsResponse struct
type GetAllServerOutputChannelSettingsResponse struct {
	Message                     string                              `json:"message"`
	ServerOutputChannelSettings []models.ServerOutputChannelSetting `json:"server_output_channel_settings"`
}

// GetServerOutputChannelSettingByIDResponse struct
type GetServerOutputChannelSettingByIDResponse struct {
	Message                    string                            `json:"message"`
	ServerOutputChannelSetting models.ServerOutputChannelSetting `json:"server_output_channel_setting"`
}

// CreateServerOutputChannelSettingResponse struct
type CreateServerOutputChannelSettingResponse struct {
	Message                    string                            `json:"message"`
	ServerOutputChannelSetting models.ServerOutputChannelSetting `json:"server_output_channel_setting"`
}

// UpdateServerOutputChannelSettingRequest struct
type UpdateServerOutputChannelSettingRequest struct {
	ServerOutputChannelID uint   `json:"server_output_channel_id"`
	SettingName           string `json:"setting_name"`
	SettingValue          string `json:"setting_value"`
	Enabled               bool   `json:"enabled"`
}

// UpdateServerOutputChannelSettingResponse struct
type UpdateServerOutputChannelSettingResponse struct {
	Message                    string                            `json:"message"`
	ServerOutputChannelSetting models.ServerOutputChannelSetting `json:"server_output_channel_settings"`
}

// DeleteServerOutputChannelSettingResponse struct
type DeleteServerOutputChannelSettingResponse struct {
	Message string `json:"message"`
	Count   int64  `json:"count"`
}
