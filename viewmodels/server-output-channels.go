package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetAllServerOutputChannelsResponse struct
type GetAllServerOutputChannelsResponse struct {
	Message              string                       `json:"message"`
	ServerOutputChannels []models.ServerOutputChannel `json:"server_output_channels"`
}

// GetServerOutputChannelByIDResponse struct
type GetServerOutputChannelByIDResponse struct {
	Message             string                     `json:"message"`
	ServerOutputChannel models.ServerOutputChannel `json:"server_output_channel"`
}

// CreateServerOutputChannelResponse struct
type CreateServerOutputChannelResponse struct {
	Message             string                     `json:"message"`
	ServerOutputChannel models.ServerOutputChannel `json:"server_output_channel"`
}

// UpdateServerOutputChannelRequest struct
type UpdateServerOutputChannelRequest struct {
	ChannelID           string `json:"channel_id"`
	OutputChannelTypeID string `json:"output_channel_type_id"`
	ServerID            uint   `json:"server_id"`
	Enabled             bool   `json:"enabled"`
}

// UpdateServerOutputChannelResponse struct
type UpdateServerOutputChannelResponse struct {
	Message             string                     `json:"message"`
	ServerOutputChannel models.ServerOutputChannel `json:"server_output_channel"`
}

// DeleteServerOutputChannelResponse struct
type DeleteServerOutputChannelResponse struct {
	Message string `json:"message"`
	Count   int64  `json:"count"`
}
