package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetAllServerTypesResponse struct
type GetAllServerTypesResponse struct {
	Message     string              `json:"message"`
	ServerTypes []models.ServerType `json:"server_types"`
}

// GetServerTypeByIDResponse struct
type GetServerTypeByIDResponse struct {
	Message    string            `json:"message"`
	ServerType models.ServerType `json:"server_type"`
}

// CreateServerTypeResponse struct
type CreateServerTypeResponse struct {
	Message    string            `json:"message"`
	ServerType models.ServerType `json:"server_type"`
}

// UpdateServerTypeRequest struct
type UpdateServerTypeRequest struct {
	Description string `json:"description"`
	Enabled     bool   `json:"enabled"`
}

// UpdateServerTypeResponse struct
type UpdateServerTypeResponse struct {
	Message    string            `json:"message"`
	ServerType models.ServerType `json:"server_type"`
}

// DeleteServerTypeResponse struct
type DeleteServerTypeResponse struct {
	Message string `json:"message"`
	Count   int64  `json:"count"`
}
