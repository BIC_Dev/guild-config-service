package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetAllServersResponse struct
type GetAllServersResponse struct {
	Message string          `json:"message"`
	Servers []models.Server `json:"servers"`
}

// GetServerByIDResponse struct
type GetServerByIDResponse struct {
	Message string        `json:"message"`
	Server  models.Server `json:"server"`
}

// CreateServerRequest struct
type CreateServerRequest struct {
	ID             int    `json:"id"`
	Name           string `json:"name"`
	ServerTypeID   string `json:"server_type_id"`
	GuildID        string `json:"guild_id"`
	NitradoTokenID string `json:"nitrado_token_id"`
	Enabled        bool   `json:"enabled"`
}

// CreateServerResponse struct
type CreateServerResponse struct {
	Message string        `json:"message"`
	Server  models.Server `json:"server"`
}

// UpdateServerRequest struct
type UpdateServerRequest struct {
	NitradoID      int    `json:"nitrado_id"`
	Name           string `json:"name"`
	ServerTypeID   string `json:"server_type_id"`
	GuildID        string `json:"guild_id"`
	NitradoTokenID uint   `json:"nitrado_token_id"`
	BoostSettingID uint   `json:"boost_setting_id"`
	Enabled        bool   `json:"enabled"`
}

// UpdateServerResponse struct
type UpdateServerResponse struct {
	Message string        `json:"message"`
	Server  models.Server `json:"server"`
}

// DeleteServerResponse struct
type DeleteServerResponse struct {
	Message string `json:"message"`
	Count   int64  `json:"count"`
}
