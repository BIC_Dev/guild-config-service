package viewmodels

import "gitlab.com/BIC_Dev/guild-config-service/models"

// GetAllSetupsResponse struct
type GetAllSetupsResponse struct {
	Message string         `json:"message"`
	Setups  []models.Setup `json:"setups"`
}

// GetSetupByIDResponse struct
type GetSetupByIDResponse struct {
	Message string       `json:"message"`
	Setup   models.Setup `json:"setup"`
}

// CreateSetupResponse struct
type CreateSetupResponse struct {
	Message string       `json:"message"`
	Setup   models.Setup `json:"setup"`
}

// UpdateSetupRequest struct
type UpdateSetupRequest struct {
	GuildID        string `json:"guild_id"`
	ContactID      string `json:"contact_id"`
	GuildServiceID uint   `json:"guild_service_id"`
}

// UpdateSetupResponse struct
type UpdateSetupResponse struct {
	Message string       `json:"message"`
	Setup   models.Setup `json:"setup"`
}

// DeleteSetupResponse struct
type DeleteSetupResponse struct {
	Message string `json:"message"`
	Count   int64  `json:"count"`
}
