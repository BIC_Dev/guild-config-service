package viewmodels

// CreateStatusSetupRequest struct
type CreateStatusSetupRequest struct {
	GuildID     string                       `json:"guild_id"`
	GuildName   string                       `json:"guild_name"`
	ContactID   string                       `json:"contact_id"`
	ContactName string                       `json:"contact_name"`
	Options     CreateNSMSetupRequestOptions `json:"options"`
}

// CreateStatusSetupRequestOptions struct
type CreateStatusSetupRequestOptions struct {
	CreateChannels  bool     `json:"create_channels"`
	NitradoAccounts []string `json:"nitrado_accounts"`
	ServerTypes     []string `json:"server_types"`
}

// CreateStatusSetupResponse struct
type CreateStatusSetupResponse struct {
	Message              string `json:"message"`
	NitradoAccountsCount int    `json:"nitrado_accounts_count"`
	ServersCount         int    `json:"servers_count"`
}

// DeleteStatusSetupResponse struct
type DeleteStatusSetupResponse struct {
	Message      string `json:"message"`
	ServersCount int    `json:"servers_count"`
}
